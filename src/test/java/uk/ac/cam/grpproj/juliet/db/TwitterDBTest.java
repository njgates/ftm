package uk.ac.cam.grpproj.juliet.db;

import static org.junit.Assert.assertTrue;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.financial.Index;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class TwitterDBTest {
	private TwitterDB tDB;

	@Before
	public void setUp() throws Exception {
		tDB = new TwitterDB();
	}

	@After
	public void tearDown() throws Exception {
		tDB.close();
	}

	@Test
	public void testGetTweetsForDateRange() throws SQLException {
		List<Tweet> tweets = tDB.getTweetsForDateRange(
				new Date(1357547400000L), new Date(1357925400000L), new Index(
						"^FTSE"));
		assertTrue("No tweets returned by getTweetsForDateRange",
				tweets.size() != 0);
	}
}
