package uk.ac.cam.grpproj.juliet.financial;

import static org.junit.Assert.assertEquals;

import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class IndexTest {

	private Index i;

	@Before
	public void setUp() throws Exception {
		i = new Index("^NDX");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		assertEquals("Couldn't get right name", "NASDAQ", i.getName());

		try {
			assertEquals("Wrong company count", 100, i.getCompanies().size());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
