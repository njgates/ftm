package uk.ac.cam.grpproj.juliet.db;

import java.sql.SQLException;
import java.util.Date;

import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.Dummy;
import uk.ac.cam.grpproj.juliet.Settings;
import uk.ac.cam.grpproj.juliet.financial.Company;
import uk.ac.cam.grpproj.juliet.financial.Index;

public class YahooDBTest {

	private YahooDB ydb;

	@Before
	public void setUp() throws Exception {
		ydb = new YahooDB();
	}

	@After
	public void tearDown() throws Exception {
		ydb.close();
	}

	@Test
	public void testGetFinancialData() throws SQLException {
		System.out.println(new Date().getTime());

		Settings s = Dummy.getDummySettings();

		XYSeries xy = ydb.getFinancialData(s.getIndex(), s.getDateFrom(),
				s.getDateTo());

		for (Object d : xy.getItems()) {
			XYDataItem i = (XYDataItem) d;
			System.out.println(new Date((long) i.getXValue()).toGMTString()
					+ " " + i.getYValue());
		}
	}

	@Test
	public void testGetAggregatedFinancialData() throws SQLException {
		Index index = new Index("^NDX");
		// dateStart = 2013-02-19 10:30:00
		Date dateStart = new Date(1361269800000L);
		// dateEnd = 2013-02-19 11:40:59
		Date dateEnd = new Date(1361274059000L);

		XYSeries xy = ydb.getAggregatedFinancialData(
				new Company(index.getTicker()), dateStart, dateEnd, 15);
		for (Object d : xy.getItems()) {
			XYDataItem i = (XYDataItem) d;
			System.out.println(new Date((long) i.getXValue()).toGMTString()
					+ " " + i.getYValue());
		}
	}
}
