package uk.ac.cam.grpproj.juliet.ui.views.graph;

import java.awt.Dimension;
import java.sql.SQLException;

import javax.swing.JFrame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.Dummy;
import uk.ac.cam.grpproj.juliet.Game;
import uk.ac.cam.grpproj.juliet.GameData;

public class InputGraphTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Thread t = new Thread(new GUI());
		t.start();
		while (true)
			;
	}

	private class GUI implements Runnable {

		public void run() {
			try {
				Game g = Dummy.getDummyGame();
				GameData gd = g.getGameData();
				InputGraph ig = new InputGraph(gd);

				JFrame f = new JFrame("Test Window");
				f.setPreferredSize(new Dimension(500, 500));
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.getContentPane().add(ig); // Add panel to test
				f.pack();
				f.setLocationRelativeTo(null);
				f.setVisible(true);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	}

}
