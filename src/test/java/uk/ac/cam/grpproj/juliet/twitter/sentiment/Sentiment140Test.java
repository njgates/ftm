package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class Sentiment140Test {

	private Sentiment140 sent;

	@Before
	public void setUp() throws Exception {
		sent = new Sentiment140();
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testAnalyseTweets() {
		List<Tweet> tweets = new LinkedList<Tweet>();

		Tweet t = new Tweet(
				1234,
				new Date(),
				1,
				10,
				"^FTSE",
				"FT UK Markets News / FTSE 100 set for best January in 13 years via FT The Financial Times Ltd 2012 http://goo.gl/Zoz9r",
				0.0);

		tweets.add(t);
		tweets.add(t);

		sent.analyseTweets(tweets);
	}

}
