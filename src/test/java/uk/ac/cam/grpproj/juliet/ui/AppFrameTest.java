package uk.ac.cam.grpproj.juliet.ui;

import java.sql.SQLException;

import javax.swing.JFrame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.Dummy;

public class AppFrameTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Thread t = new Thread(new GUI());
		t.start();
		while (true)
			;
	}

	private class GUI implements Runnable, FrameDelegate {

		public void run() {
			AppFrame f = null;
			try {
				f = new AppFrame(Dummy.getDummyGame(), this);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			f.setVisible(true);
		}

		@Override
		public void didClickSubmit(JFrame sender) {
		}

		@Override
		public void didClickCancel(JFrame sender) {
			System.exit(0);
		}
	}

}
