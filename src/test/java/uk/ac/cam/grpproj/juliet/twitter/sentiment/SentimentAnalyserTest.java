package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.db.TwitterDB;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class SentimentAnalyserTest implements SentimentDelegate {

	private boolean running = true;

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws SQLException {
		SentimentAnalyser sa = new SentimentAnalyser(new Sentiment140(), this);
		TwitterDB tdb = new TwitterDB();
		List<Tweet> ts = tdb.getUnanalysedTweets();
		System.out.println("Analysing " + ts.size() + " tweets.");
		tdb.close();
		if (ts.size() > 0)
			sa.analyseTweets(ts);
		else
			running = false;
		while (running)
			;
	}

	@Override
	public void didFinishSentimentAnalysis(List<Tweet> tweets) {
		TwitterDB tdb = null;
		try {
			tdb = new TwitterDB();
			tdb.updateTweetSentiment(tweets);
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {
			tdb.close();
		}
		System.out.println("Done");
		running = false;
	}

}
