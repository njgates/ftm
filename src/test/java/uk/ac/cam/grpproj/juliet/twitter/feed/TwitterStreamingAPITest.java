package uk.ac.cam.grpproj.juliet.twitter.feed;

import java.io.IOException;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.db.CompanyDB;

public class TwitterStreamingAPITest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws SQLException, IOException {
		CompanyDB cdb = new CompanyDB();
		new Thread(new TwitterStreamingAPI(cdb.getIndices())).start();
		cdb.close();
		while (true)
			;
	}

}
