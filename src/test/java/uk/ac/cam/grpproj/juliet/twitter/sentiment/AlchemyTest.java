package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class AlchemyTest {

	private Alchemy sent;

	@Before
	public void setUp() throws Exception {
		sent = new Alchemy();
	}

	@After
	public void tearDown() throws Exception {

	}

	@Test
	public void testAnalyseTweets() {
		List<Tweet> tweets = new LinkedList<Tweet>();

		Tweet t = new Tweet(1234, new Date(), 1, 10, "^FTSE",
				"FTSE is really bad", 0.0);

		tweets.add(t);
		tweets.add(t);

		sent.analyseTweets(tweets);

		System.out.println(tweets.get(0).getSentiment());
	}

}
