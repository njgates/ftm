package uk.ac.cam.grpproj.juliet.db;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.financial.Index;

public class CompanyDBTest {

	private CompanyDB comp;

	@Before
	public void setUp() throws Exception {
		comp = new CompanyDB();
	}

	@After
	public void tearDown() throws Exception {
		comp.close();
	}

	@Test
	public void testGetIndices() {
		try {
			List<Index> indices = comp.getIndices();
			Index i = indices.get(0);
			if (i.getTicker().equals("^FTSE")) {
				assertEquals("Index name incorrect", "FTSE 100", i.getName());
			}
			if (i.getTicker().equals("^NDX")) {
				assertEquals("Index name incorrect", "NASDAQ 100", i.getName());
			}
		} catch (SQLException e) {
			e.printStackTrace();
			fail("SQL Exception");
		}
	}

	@Test
	public void testGetNameOfIndex() {
		try {
			assertEquals("Index name incorrect", "FTSE 100",
					comp.getNameOfIndex("^FTSE"));
		} catch (SQLException e) {
			e.printStackTrace();
			fail("SQL Exception");
		}
	}
}
