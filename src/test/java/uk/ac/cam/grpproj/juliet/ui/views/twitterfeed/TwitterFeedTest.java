package uk.ac.cam.grpproj.juliet.ui.views.twitterfeed;

import java.awt.Dimension;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.db.TwitterDB;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class TwitterFeedTest {

	List<Tweet> tweets;

	@Before
	public void setUp() throws Exception {
		tweets = new LinkedList<Tweet>();
		tweets.add(new Tweet(
				1234,
				new Date(),
				"String1",
				"Until a component is displayable, its graphics will be null. This is a real hassle because there is no easy way to access a FontMetrics without a Graphics.",
				0));
		tweets.add(new Tweet(1234, new Date(), "String1", "String 2", 0));
		tweets.add(new Tweet(1234, new Date(), "String1", "String 2", 0));
		tweets.add(new Tweet(1234, new Date(), "String1", "String 2", 0));

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Thread t = new Thread(new GUI());
		t.start();
		while (true)
			;
	}

	private class GUI implements Runnable {
		public void run() {
			try {

				TwitterDB tdb = new TwitterDB();
				final TwitterFeed tf = new TwitterFeed(tweets);
				tdb.close();
				JFrame f = new JFrame("Test Window");
				f.setPreferredSize(new Dimension(500, 500));
				f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				f.getContentPane().add(tf); // Add panel to test
				f.pack();
				f.setLocationRelativeTo(null);
				f.setVisible(true);

			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}

}
