package uk.ac.cam.grpproj.juliet.ui.views.graph.components;

import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.UIManager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.cam.grpproj.juliet.ui.Theme;
import uk.ac.cam.grpproj.juliet.ui.components.FTMButtonUI;

public class FTMComboBoxTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() {
		Thread t = new Thread(new GUI());
		t.start();
		while (true)
			;
	}

	private class GUI implements Runnable {

		public void run() {
			JFrame f = new JFrame("Test Window");
			f.setPreferredSize(new Dimension(500, 500));
			f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			UIManager.put("ButtonUI", FTMButtonUI.class.getName());
			f.setBackground(Theme.bgColor);

			FTMComboBox<String> cb = new FTMComboBox<String>();
			cb.addItem("Hello");
			cb.addItem("Hello2");

			f.getContentPane().add(cb); // Add panel to test
			f.pack();
			f.setLocationRelativeTo(null);
			f.setVisible(true);
		}
	}

}
