package uk.ac.cam.grpproj.juliet;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InvalidClassException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import uk.ac.cam.grpproj.juliet.DataPoints.IllegalIntervalException;
import uk.ac.cam.grpproj.juliet.db.TwitterDB;
import uk.ac.cam.grpproj.juliet.db.YahooDB;
import uk.ac.cam.grpproj.juliet.financial.Company;
import uk.ac.cam.grpproj.juliet.financial.Index;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;
import uk.ac.cam.grpproj.juliet.twitter.sentiment.ProcessSentiment;

public class Dummy {

	public static Settings getDummySettings() {
		Settings s = new Settings();

		try {
			// 7th to 11th January
			// s.setDateFrom(new Date(1357547400000L));
			// s.setDateTo(new Date(1357925400000L));
			// s.setIndex(new Index("^FTSE"));

			// Use real tweets
			s.setDateFrom(new Date(1360933200000L));
			s.setDateTo(new Date(1360947600000L));
			s.setIndex(new Index("^NDX"));

			// User more real tweets
			// s.setDateFrom(new Date(1361289600000L));
			// /s.setDateTo(new Date(1361295600000L));
			// s.setIndex(new Index("^NDX"));

			s.setMode(GameMode.GAME);
		} catch (SQLException e) {
			e.printStackTrace();
			System.exit(0);
		}

		return s;
	}

	public static GameData getDummyGameData(Settings s) throws SQLException,
			IllegalIntervalException {
		GameData d = new GameData();

		YahooDB ydb = new YahooDB();
		System.out.println("Getting Actual Market Data...");
		d.setMarketActual(ydb.getAggregatedFinancialData(new Company(s
				.getIndex().getTicker()), s.getDateFrom(), s.getDateTo(), 5));
		ydb.close();

		System.out.println("Getting Tweets...");
		d.setTweets(getDummyTweets(s));

		System.out.println("Computing Sentiment...");
		d.setSentiment(ProcessSentiment.getSentimentSeries(d.getMarketActual(),
				s.getIndex()));

		return d;
	}

	public static Game getDummyGame(boolean forceNew) throws SQLException {

		// Check for local file, if exists, load that!
		File f = new File("gameData.dat");
		Game g = null;

		try {
			if (!forceNew && f.exists()) {
				try {
					ObjectInputStream ois = new ObjectInputStream(
							new FileInputStream(f));
					g = (Game) ois.readObject();
					ois.close();
				} catch (InvalidClassException e) {
					f.delete();
					System.out.println("File removed for update, please retry");
					System.exit(0);
				}
			} else {
				Settings s = getDummySettings();
				g = new Game(s, getDummyGameData(s));
				ObjectOutputStream oos = new ObjectOutputStream(
						new FileOutputStream(f));
				oos.writeObject(g);
				oos.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return g;
	}

	public static Game getDummyGame() throws SQLException {
		return getDummyGame(false);
	}

	public static List<Tweet> getDummyTweets(Settings s) throws SQLException {
		TwitterDB tdb = new TwitterDB();
		List<Tweet> tweets = tdb.getBestTweetsForDateRange(s.getDateFrom(),
				s.getDateTo(), s.getIndex(), 100);
		tdb.close();
		return tweets;
	}
}
