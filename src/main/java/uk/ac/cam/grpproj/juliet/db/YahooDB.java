package uk.ac.cam.grpproj.juliet.db;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.jfree.data.xy.XYSeries;

import uk.ac.cam.grpproj.juliet.financial.Company;
import uk.ac.cam.grpproj.juliet.financial.Index;
import uk.ac.cam.grpproj.juliet.financial.Price;

public class YahooDB extends DatabaseHelper {

	public YahooDB() throws SQLException {
		super();
	}

	/**
	 * Writes the details of a List of Prices into the database
	 * 
	 * @param listOfPrices
	 *            List of Prices
	 * @see Price
	 */
	public void writeFinancialData(List<Price> listOfPrices)
			throws SQLException, MalformedURLException, IOException {
		String sql = "INSERT INTO "
				+ DatabaseConfig.getFinancialDataTableName()
				+ " (date,company_ticker,open,high,low,close) VALUES (?,?,?,0,0,0)";
		PreparedStatement ps = connection.prepareStatement(sql);
		try {
			for (Price p : listOfPrices) {
				// Set date
				ps.setString(1, convertDateToMySQLDateTime(p.getDate()));
				// Set ticker
				ps.setString(2, p.getTicker());
				// Set opens (leaving the rest of the columns blank)
				ps.setDouble(3, p.getPrice());
				ps.execute();
			}
		} finally {
			ps.close();
		}
	}

	/**
	 * Returns all available financial data for an index within a given range
	 * 
	 * @param index
	 *            Index for which financial data is required
	 * @param dateStart
	 *            Start date for the date range
	 * @param dateEnd
	 *            End date for the date range
	 * @return XYSeries Financial data points.
	 * @see Index
	 * @see XYSeries
	 */
	/*
	 * public XYSeries getFinancialData(Index index, Date dateStart, Date
	 * dateEnd) throws SQLException { PreparedStatement stmt = connection
	 * .prepareStatement("SELECT company_ticker, open, date FROM " +
	 * DatabaseConfig.getFinancialDataTableName() +
	 * " WHERE company_ticker = ? AND date BETWEEN ? AND ? ORDER BY date DESC");
	 * stmt.setString(1, index.getTicker()); stmt.setString(2,
	 * convertDateToMySQLDateTime(dateStart)); stmt.setString(3,
	 * convertDateToMySQLDateTime(dateEnd));
	 * 
	 * ResultSet results = stmt.executeQuery(); XYSeries priceSeries = new
	 * XYSeries(index.getName()); while (results.next()) { try { Date date =
	 * convertMySQLDateTimeToDate(results .getString("date"));
	 * priceSeries.add(date.getTime(), results.getDouble("open")); } catch
	 * (ParseException e) { // Couldn't parse date so just ignore point
	 * System.out.println("Couldn't parse date"); } } return priceSeries; }
	 */

	/**
	 * Returns averaged financial data for an index, partitioned into time
	 * intervals within a given range
	 * 
	 * @param index
	 *            Index for which financial data is required
	 * @param dateStart
	 *            Start date for the date range
	 * @param dateEnd
	 *            End date for the date range
	 * @param timeInterval
	 *            Time interval length in minutes
	 * @return XYSeries Averaged financial data points.
	 * @see Index
	 * @see XYSeries
	 */
	public XYSeries getAggregatedFinancialData(Company company, Date dateStart,
			Date dateEnd, int timeInterval) throws SQLException {
		String start = convertDateToMySQLDateTime(dateStart);
		String end = convertDateToMySQLDateTime(dateEnd);
		String dbTable = DatabaseConfig.getFinancialDataTableName();
		PreparedStatement stmt = connection
				.prepareStatement("SELECT company_ticker," + " AVG(" + dbTable
						+ ".open) AS open," + " ((UNIX_TIMESTAMP(" + dbTable
						+ ".date) - UNIX_TIMESTAMP(?)) DIV ?) AS date_interval"
						+ " FROM " + dbTable + " WHERE company_ticker = ?"
						+ " AND " + dbTable + ".date BETWEEN ? AND ?"
						+ " GROUP BY date_interval");
		stmt.setString(1, start);
		stmt.setInt(2, timeInterval * 60);
		stmt.setString(3, company.getTicker());
		stmt.setString(4, start);
		stmt.setString(5, end);

		ResultSet results = stmt.executeQuery();
		XYSeries priceSeries = new XYSeries(company.getName());
		while (results.next()) {
			int date_interval = results.getInt("date_interval");
			Date date = new Date(dateStart.getTime() + date_interval
					* (timeInterval * 60000L));
			priceSeries.add(date.getTime(), results.getDouble("open"));
		}
		System.out.println("Got " + priceSeries.getItemCount()
				+ " financial datapoints");
		return priceSeries;
	}
}
