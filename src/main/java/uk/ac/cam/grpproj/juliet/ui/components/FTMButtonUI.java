package uk.ac.cam.grpproj.juliet.ui.components;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicButtonUI;

import uk.ac.cam.grpproj.juliet.ui.Theme;

public class FTMButtonUI extends BasicButtonUI {

	private static Rectangle iconRect = new Rectangle();

	public static ComponentUI createUI(JComponent c) {
		return new FTMButtonUI();
	}

	@Override
	public void paint(Graphics g, JComponent c) {
		JButton b = (JButton) c;
		b.setContentAreaFilled(false);

		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		// Create border
		g2.setPaint(new GradientPaint(new Point(0, 0), Theme.borderGradTop,
				new Point(0, c.getHeight()), Theme.borderGradBottom));
		g2.fillRoundRect(0, 0, c.getWidth(), c.getHeight(), Theme.borderRadius,
				Theme.borderRadius);

		if (b.getModel().isPressed())
			paintButtonPressed(g2, b);
		else
			paintButtonUnpressed(g2, b);

		// Paint the Icon
		if (b.getIcon() != null) {
			paintIcon(g, c, iconRect);
		}

		// Paint text
		this.paintText(g2, b);
	}

	private void paintButtonUnpressed(Graphics2D g2, AbstractButton b) {
		// Create gradient BG
		g2.setPaint(new GradientPaint(new Point(0, 0), Theme.topGradient,
				new Point(0, b.getHeight()), Theme.bottomGradient));
		g2.fillRoundRect(Theme.borderThickness, Theme.borderThickness,
				b.getWidth() - (2 * Theme.borderThickness), b.getHeight()
						- (2 * Theme.borderThickness), Theme.borderRadius
						- Theme.borderThickness, Theme.borderRadius
						- Theme.borderThickness);

		// Create highlight
		g2.setPaint((Paint) Theme.highlight);
		g2.drawLine(1 + Theme.borderThickness, Theme.borderThickness,
				b.getWidth() - (2 * Theme.borderThickness),
				Theme.borderThickness);
	}

	private void paintButtonPressed(Graphics2D g2, AbstractButton b) {
		// Create flat BG
		g2.setPaint((Paint) Theme.pressed);
		g2.fillRoundRect(Theme.borderThickness, Theme.borderThickness,
				b.getWidth() - (2 * Theme.borderThickness), b.getHeight()
						- (2 * Theme.borderThickness), Theme.borderRadius
						- Theme.borderThickness, Theme.borderRadius
						- Theme.borderThickness);
	}

	private void paintText(Graphics g, AbstractButton b) {

		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		// Add text
		// Find the size of string s in font f in the current Graphics context
		// g.
		FontMetrics fm = g.getFontMetrics(g2.getFont());
		java.awt.geom.Rectangle2D rect = fm.getStringBounds(b.getText(), g);

		int textHeight = (int) (rect.getHeight());
		int textWidth = (int) (rect.getWidth());
		int panelHeight = b.getHeight();
		int panelWidth = b.getWidth();

		// Center text horizontally and vertically
		int x = (panelWidth - textWidth) / 2;
		int y = (panelHeight - textHeight) / 2 + fm.getAscent();

		g2.setPaint((Paint) Color.black);
		g2.drawString(b.getText(), x + 1, y + 1);

		if (b.isEnabled())
			g2.setPaint(Theme.textColor);
		else
			g2.setPaint(Theme.textDisabled);
		g2.drawString(b.getText(), x, y);
	}
}
