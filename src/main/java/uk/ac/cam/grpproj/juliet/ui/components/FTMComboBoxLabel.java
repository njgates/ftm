package uk.ac.cam.grpproj.juliet.ui.components;

import javax.swing.JLabel;

public class FTMComboBoxLabel extends JLabel {
	private static final long serialVersionUID = 1006485734119670912L;

	private boolean selected = false;

	FTMComboBoxLabel(String text) {
		super(text);
		this.setUI(new FTMLabelUI());
	}

	public FTMComboBoxLabel() {
		this.setUI(new FTMLabelUI());
	}

	public void setSelected(boolean s) {
		selected = s;
	}

	public boolean isSelected() {
		return selected;
	}

}
