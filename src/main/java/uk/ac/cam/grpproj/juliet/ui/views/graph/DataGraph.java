package uk.ac.cam.grpproj.juliet.ui.views.graph;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.labels.StandardXYToolTipGenerator;
import org.jfree.data.xy.XYSeries;

import uk.ac.cam.grpproj.juliet.Game;
import uk.ac.cam.grpproj.juliet.GameData;
import uk.ac.cam.grpproj.juliet.ui.PlaybackPanel;

public class DataGraph extends JPanel implements PlaybackPanel {
	private static final long serialVersionUID = -1527322230562844959L;

	private ChartPanel panel;
	private JFreeChart chart;

	DynamicXYSeriesCollection financialDataset;
	DynamicXYSeriesCollection sentimentDataset;

	StandardXYToolTipGenerator dataTooltip;

	GameData data;
	Game game;

	private SimpleDateFormat d = new SimpleDateFormat("EEE, MMM d, ''yy");
	private NumberFormat numFormat = new DecimalFormat();

	public DataGraph(Game game) {
		this.data = game.getGameData();
		this.game = game;

		// Set up financial dataset
		financialDataset = new DynamicXYSeriesCollection();
		financialDataset.addSeries(data.getMarketPredicted(), new XYSeries(
				"Predicted"));
		financialDataset.addSeries(data.getMarketActual(), new XYSeries(game
				.getSettings().getIndex().getName()));

		// Set up sentiment dataset
		sentimentDataset = new DynamicXYSeriesCollection();
		sentimentDataset.addSeries(data.getSentiment(), new XYSeries(
				"Sentiment"));

		chart = GraphFactory.createDataGraph("Time", "Price", financialDataset,
				data);
		chart.getXYPlot().setDataset(1, sentimentDataset);
		this.setLayout(new MigLayout("nogrid, fill, insets 0"));

		panel = new ChartPanel(chart);
		panel.setDomainZoomable(false);
		panel.setRangeZoomable(false);
		panel.setPopupMenu(null);

		this.add(panel, "grow, id graph");

		// For tooltips
		numFormat.setMaximumFractionDigits(3);
		dataTooltip = new StandardXYToolTipGenerator("%s", d, numFormat);

	}

	@Override
	public void playbackResumed() {
		financialDataset.playbackResumed();
		sentimentDataset.playbackResumed();
	}

	@Override
	public void playbackStarted() {
		financialDataset.playbackStarted();
		sentimentDataset.playbackStarted();
	}

	@Override
	public void playbackStopped() {
		financialDataset.playbackStopped();
		sentimentDataset.playbackStopped();
	}

	@Override
	public void updatePanelToDate(Date date) {
		financialDataset.updatePanelToDate(date);
		sentimentDataset.updatePanelToDate(date);
	}

}
