package uk.ac.cam.grpproj.juliet.ui.views.playback;

public interface PlaybackDelegate {

	public void didPlay();

	public void didStop();

	public void didPause();

	public void didChangeSpeed(int speed);

}
