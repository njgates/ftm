package uk.ac.cam.grpproj.juliet.ui.components;

import java.awt.Component;

import javax.swing.JList;
import javax.swing.ListCellRenderer;

import net.miginfocom.swing.MigLayout;
import uk.ac.cam.grpproj.juliet.ui.Theme;

public class FTMComboBoxRenderer<T> extends FTMComboBoxLabel implements
		ListCellRenderer<T> {
	private static final long serialVersionUID = 1L;

	public FTMComboBoxRenderer() {
		// super();
		this.setLayout(new MigLayout("insets 16"));
		setBackground(Theme.comboBG);
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends T> list,
			T value, int index, boolean isSelected, boolean cellHasFocus) {

		setText(value.toString());

		if (isSelected) {
			setSelected(true);
		} else {
			setSelected(false);
		}

		return this;
	}
}