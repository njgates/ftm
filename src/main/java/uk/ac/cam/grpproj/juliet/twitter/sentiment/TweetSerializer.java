package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.lang.reflect.Type;

import uk.ac.cam.grpproj.juliet.twitter.Tweet;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

public class TweetSerializer implements JsonSerializer<Tweet> {

	public JsonElement serialize(Tweet t, Type type,
			JsonSerializationContext context) {
		JsonObject jo = new JsonObject();
		jo.addProperty("text", t.getText());
		return jo;
	}

}
