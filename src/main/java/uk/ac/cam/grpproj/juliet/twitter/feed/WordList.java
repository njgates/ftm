package uk.ac.cam.grpproj.juliet.twitter.feed;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;

public class WordList {

	private static WordList wl = null;
	private Set<String> words = new HashSet<String>();

	public WordList() {
		BufferedReader r = new BufferedReader(new InputStreamReader(getClass()
				.getClassLoader().getResourceAsStream("words.txt")));
		String s;
		try {
			while ((s = r.readLine()) != null)
				words.add(s);
		} catch (IOException e) {
			System.err.println("No dictionary file located.");
		}
	}

	public static WordList getInstance() {
		if (wl == null)
			wl = new WordList();
		return wl;
	}

	public boolean isWordEnglish(String word) {
		boolean y = words.contains(word.toLowerCase());
		return y;
	}

}
