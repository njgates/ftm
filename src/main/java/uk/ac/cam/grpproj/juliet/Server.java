package uk.ac.cam.grpproj.juliet;

import java.sql.SQLException;

import uk.ac.cam.grpproj.juliet.db.CompanyDB;
import uk.ac.cam.grpproj.juliet.financial.YahooDataFetcher;
import uk.ac.cam.grpproj.juliet.twitter.feed.TwitterStreamingAPI;
import uk.ac.cam.grpproj.juliet.twitter.sentiment.SentimentAggregateThread;

public class Server {
	public void runServer(String[] args) {

		// Non-blocking call to start Twitter Stream Fetcher
		// FIXME: need to start twitter streams when market opens
		// and kill them when market closes
		// Note market open and close at different times!

		try {
			CompanyDB cdb = new CompanyDB();
			new Thread(new TwitterStreamingAPI(cdb.getIndices())).start();
			cdb.close();

			(new SentimentAggregateThread()).start();

			int minutes = 5;

			YahooDataFetcher ydb = new YahooDataFetcher(minutes);
			ydb.startYahooDataFetcher();
		} catch (SQLException e) {
			System.err.println(e.getMessage());
			// TODO Restart server?

		}
	}
}
