package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import uk.ac.cam.grpproj.juliet.helper.HttpUtility;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class Alchemy extends SentimentAlgorithm {
	private String apiURL = "http://access.alchemyapi.com/calls/text/TextGetTextSentiment";
	private String apiKey = "21e21973f13d1e34c245e795dd7d4bec67ba6d7b";

	@Override
	public void analyseTweets(List<Tweet> tweets) {
		Map<String, String> params;
		for (Tweet t : tweets) {
			params = new HashMap<String, String>();
			params.put("apikey", apiKey);
			params.put("text", t.getText());
			// params.put("showSourceText", "1");
			getSentimentOfTweet(t, params);
		}
	}

	private void getSentimentOfTweet(Tweet tweet, Map<String, String> params) {
		String[] response = { "" };
		try {
			HttpUtility.sendPostRequest(apiURL, params);
			response = HttpUtility.readMultipleLinesResponse();

			// 8 is the response line we're looking for
			// Strip xml tags
			double sentiment = Double.parseDouble(response[8].replaceAll(
					"\\<.*?\\>", ""));
			tweet.setSentiment(sentiment);
		} catch (IOException ex) {
			System.out.println("Could not connect to API to get sentiment.");
		} catch (NumberFormatException ex) {
			System.out.println("Could not get sentiment from response:");
			// Not enough detail perhaps? API will therefore return neutral
			tweet.setSentiment(0.0);
		} finally {
			HttpUtility.disconnect();
		}
	}
}
