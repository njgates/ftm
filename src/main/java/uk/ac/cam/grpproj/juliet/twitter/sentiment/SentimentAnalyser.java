package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.util.List;

import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class SentimentAnalyser {

	private SentimentAlgorithm sentAlgo;
	private SentimentDelegate delegate;

	public SentimentAnalyser(SentimentAlgorithm sentAlgo,
			SentimentDelegate delegate) {
		this.sentAlgo = sentAlgo;
		this.delegate = delegate;
	}

	public void analyseTweets(List<Tweet> tweets) {
		new Thread(new Analyse(tweets)).start();
	}

	private class Analyse extends Thread {
		private List<Tweet> tweets;

		public Analyse(List<Tweet> tweets) {
			this.tweets = tweets;
		}

		@Override
		public void run() {
			sentAlgo.analyseTweets(tweets);
			delegate.didFinishSentimentAnalysis(tweets);
		}
	}
}
