package uk.ac.cam.grpproj.juliet.ui.views.twitterfeed;

import java.util.HashMap;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;

import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class AnimatedTweetList extends JList<Tweet> implements ListDataListener {
	private static final long serialVersionUID = 7025460640218371161L;

	// Animation 200 ms long
	private final int animationDuration = 2000;
	// 25 frames per second?
	private final int animationRefresh = 40;

	private final float startSaturation = 1.0f;
	private final float endSaturation = 0.4f;

	private HashMap<Tweet, Float> saturationMap = new HashMap<Tweet, Float>();

	public AnimatedTweetList(DefaultListModel<Tweet> model) {
		super(model);
		model.addListDataListener(this);
		this.setCellRenderer(new TwitterFeedCellRenderer());
	}

	public HashMap<Tweet, Float> getSaturationMap() {
		return this.saturationMap;
	}

	@Override
	public void intervalAdded(ListDataEvent e) {
		// Check that only 1 item was added.
		if (e.getIndex0() == e.getIndex1()) {
			int index = e.getIndex0();
			CellAnimator ca = new CellAnimator(this.getModel().getElementAt(
					index));
			ca.start();
		}
		// Else load all but don't animate
		// TODO don't animate when loading multiple tweets
	}

	private class CellAnimator extends Thread {
		private long startTime;
		private long stopTime;
		private final Tweet tweet;

		public CellAnimator(Tweet t) {
			this.tweet = t;
		}

		@Override
		public void run() {
			startTime = System.currentTimeMillis();
			stopTime = startTime + animationDuration;
			saturationMap.put(tweet, startSaturation);
			while (System.currentTimeMillis() < stopTime) {
				colorizeTweet();
				repaint();
				try {
					Thread.sleep(animationRefresh);
				} catch (InterruptedException ie) {
				}
			}
			// one more, at 100% selected color
			colorizeTweet();
			repaint();
		}

		private void colorizeTweet() {
			// calculate % completion relative to start/stop times
			float elapsed = (float) (System.currentTimeMillis() - startTime);
			float completeness = Math.min((elapsed / animationDuration), 1.0f);
			// calculate scaled color
			saturationMap
					.put(tweet,
							endSaturation
									+ ((startSaturation - endSaturation) * scale(completeness)));
		}

		private float scale(float completeness) {
			return (float) (-Math.pow(completeness, 6) + 1);
		}
	}

	@Override
	public void intervalRemoved(ListDataEvent e) {
	}

	@Override
	public void contentsChanged(ListDataEvent e) {
	}
}
