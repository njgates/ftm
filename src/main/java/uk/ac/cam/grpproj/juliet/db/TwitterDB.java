package uk.ac.cam.grpproj.juliet.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import uk.ac.cam.grpproj.juliet.financial.Index;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;
import uk.ac.cam.grpproj.juliet.twitter.TwitterUser;

public class TwitterDB extends DatabaseHelper {
	public TwitterDB() throws SQLException {
		super();
	}

	/**
	 * This method is called by the Client when requesting tweets for a certain
	 * date range.
	 * 
	 * @param start
	 *            Start date for the date range
	 * @param end
	 *            End date for the date range
	 * @param index
	 *            Index for which to get tweets
	 * @return List<Tweet> A list of tweet objects
	 * @see Tweet
	 * @see Index
	 */
	public List<Tweet> getTweetsForDateRange(Date start, Date end, Index index)
			throws SQLException {
		PreparedStatement stmt = connection
				.prepareStatement("SELECT tweet_id,created_at,text,sentiment,screen_name"
						+ " FROM "
						+ DatabaseConfig.getTweetsTableName()
						+ " INNER JOIN "
						+ DatabaseConfig.getCompaniesTableName()
						+ " ON company_ticker = ticker"
						+ " LEFT JOIN "
						+ DatabaseConfig.getTwitterUsersTableName()
						+ " ON twitter_user_id = twitter_id"
						+ " WHERE index_ticker = ? "
						+ " AND created_at BETWEEN ? AND ? ORDER BY created_at ASC");
		stmt.setString(1, index.getTicker());
		stmt.setString(2, convertDateToMySQLDateTime(start));
		stmt.setString(3, convertDateToMySQLDateTime(end));

		ResultSet rs = stmt.executeQuery();

		List<Tweet> tweetList = new ArrayList<Tweet>();
		while (rs.next()) {
			tweetList.add(getTweetFromResultSet(rs));
		}
		return tweetList;
	}

	/**
	 * Returns the average sentiment for a given index and date range
	 * 
	 * @param start
	 *            Start date for the date range
	 * @param end
	 *            End date for the date range
	 * @param index
	 *            Index for which to get sentiment
	 * @return double Average sentiment
	 * @see Index
	 **/
	public Double getAverageSentimentForDateRange(Date start, Date end,
			Index index) throws SQLException {
		PreparedStatement stmt = connection
				.prepareStatement("SELECT AVG(sentiment)" + " FROM "
						+ DatabaseConfig.getTweetsTableName() + " INNER JOIN "
						+ DatabaseConfig.getCompaniesTableName()
						+ " ON company_ticker = ticker"
						+ " WHERE index_ticker = ? "
						+ " AND created_at BETWEEN ? AND ? AND sentiment != 0");
		stmt.setString(1, index.getTicker());
		stmt.setString(2, convertDateToMySQLDateTime(start));
		stmt.setString(3, convertDateToMySQLDateTime(end));

		ResultSet rs = stmt.executeQuery();
		rs.next();

		return rs.getDouble("AVG(sentiment)");
	}

	/**
	 * Returns the tweet corresponding to a given id.
	 * 
	 * @param id
	 * @return Tweet
	 * @see Tweet
	 **/
	public Tweet getTweetForID(long id) throws SQLException {
		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM "
				+ DatabaseConfig.getTweetsTableName() + " WHERE id = ?");
		stmt.setLong(1, id);

		ResultSet rs = stmt.executeQuery();
		rs.first();

		Tweet tweet = getTweetFromResultSet(rs);
		return tweet;
	}

	/**
	 * This method is called by the Server when inserting tweets in bulk to the
	 * database.
	 * 
	 * @param tweets
	 *            A list of tweet objects populated by the TwitterStreamingAPI.
	 *            This includes a TwitterUser object which has to be inserted
	 *            first for foreign key constraints to hold.
	 * @return void
	 * @see Tweet
	 * @see TwitterUser
	 */
	public void insertBulkTweets(List<Tweet> tweets) throws SQLException {
		Date now = new Date();
		System.out.println("Start bulk update");

		// Users
		String sql = "INSERT INTO "
				+ DatabaseConfig.getTwitterUsersTableName()
				+ " (twitter_id, screen_name, follower_count, location) VALUES ";
		for (int n = 0; n < tweets.size(); n++)
			sql += "(?,?,?,?), ";
		sql = sql.substring(0, sql.length() - 2);

		sql += " ON DUPLICATE KEY UPDATE follower_count=VALUES(follower_count),location=VALUES(location);";

		PreparedStatement p = connection.prepareStatement(sql);
		int i = 1;
		for (Tweet t : tweets) {
			p.setLong(i++, t.getTwitterUser().getID());
			p.setString(i++, t.getTwitterUser().getScreenName());
			p.setLong(i++, t.getTwitterUser().getFollowerCount());
			p.setString(i++, t.getTwitterUser().getLocation());
		}
		p.execute();

		// Tweets
		sql = " INSERT INTO "
				+ DatabaseConfig.getTweetsTableName()
				+ " (tweet_id, created_at, twitter_user_id, retweets, company_ticker, text, sentiment) VALUES ";
		for (i = 0; i < tweets.size(); i++)
			sql += "(?,?,?,?,?,?,?), ";
		sql = sql.substring(0, sql.length() - 2);

		sql += " ON DUPLICATE KEY UPDATE retweets=VALUES(retweets);";

		p = connection.prepareStatement(sql);
		i = 1;
		for (Tweet tweet : tweets) {
			p.setLong(i++, tweet.getID());
			p.setString(i++, convertDateToMySQLDateTime(tweet.getCreatedAt()));
			p.setLong(i++, tweet.getTwitterUserID());
			p.setLong(i++, tweet.getRetweets());
			p.setString(i++, tweet.getCompanyTicker());
			p.setString(i++, tweet.getText());
			if (tweet.getSentiment() == null)
				p.setNull(i++, Types.DOUBLE);
			else
				p.setDouble(i++, tweet.getSentiment());
		}
		p.execute();

		System.out.println("Finish bulk update of " + tweets.size() + " in "
				+ ((new Date().getTime()) - now.getTime()) + " ms");
	}

	/**
	 * This method retrieves all tweets from the database which haven't yet been
	 * analysed. These are identified by the SQL value Double.NULL
	 * 
	 * @return List<Tweet> List of tweets to be analysed. These have only a
	 *         tweet_id and text value set
	 * @see Tweet
	 */
	public List<Tweet> getUnanalysedTweets() throws SQLException {
		PreparedStatement stmt = connection
				.prepareStatement("SELECT tweet_id,text FROM "
						+ DatabaseConfig.getTweetsTableName()
						+ " WHERE sentiment IS NULL");

		ResultSet rs = stmt.executeQuery();

		List<Tweet> tweetList = new ArrayList<Tweet>();
		while (rs.next()) {
			Tweet t = new Tweet(rs.getLong("tweet_id"));
			t.setText(rs.getString("text"));
			tweetList.add(t);
		}
		return tweetList;
	}

	/**
	 * This method takes a list of Tweet object which only need their tweet_id
	 * and sentiment fields set and updates the sentiment values for those
	 * tweets in the database.
	 * 
	 * @param tweets
	 *            a List of Tweet objects
	 * @return void
	 * @see Tweet
	 */
	public void updateTweetSentiment(List<Tweet> tweets) throws SQLException {
		String sql = "UPDATE " + DatabaseConfig.getTweetsTableName()
				+ " SET sentiment = CASE tweet_id ";
		for (int i = 0; i < tweets.size(); i++)
			sql += "WHEN ? THEN ? ";
		sql += "END WHERE tweet_id IN (";
		for (int i = 0; i < tweets.size(); i++)
			sql += "? ,";
		sql = sql.substring(0, sql.length() - 1);
		sql += ")";

		PreparedStatement p = connection.prepareStatement(sql);
		int i = 1;
		for (Tweet tweet : tweets) {
			p.setLong(i++, tweet.getID());
			p.setDouble(i++, tweet.getSentiment());
		}
		for (Tweet tweet : tweets) {
			p.setLong(i++, tweet.getID());
		}
		p.execute();
	}

	public TwitterUser getTwitterUser(long twitter_user_id) throws SQLException {
		PreparedStatement stmt = connection.prepareStatement("SELECT * FROM "
				+ DatabaseConfig.getTwitterUsersTableName()
				+ " WHERE twitter_id = ?");
		stmt.setLong(1, twitter_user_id);

		ResultSet rs = stmt.executeQuery();
		rs.first();

		TwitterUser user = new TwitterUser(rs.getLong("twitter_id"),
				rs.getString("screen_name"), rs.getLong("follower_count"),
				rs.getString("location"));
		return user;
	}

	/*
	 * public void updateOrInsertTwitterUser(TwitterUser twitter_user) throws
	 * SQLException { PreparedStatement stmt =
	 * connection.prepareStatement("INSERT INTO " +
	 * DatabaseConfig.getTwitterUsersTableName() +
	 * " (twitter_id, screen_name, follower_count, location) " +
	 * " VALUES (?, ?, ?, ?)" +
	 * " ON DUPLICATE KEY UPDATE follower_count = ?, location = ?");
	 * stmt.setLong(1, twitter_user.getID()); stmt.setString(2,
	 * twitter_user.getScreenName()); stmt.setLong(3,
	 * twitter_user.getFollowerCount()); stmt.setLong(5,
	 * twitter_user.getFollowerCount()); stmt.setString(4,
	 * twitter_user.getLocation()); stmt.setString(6,
	 * twitter_user.getLocation());
	 * 
	 * stmt.executeUpdate(); }
	 */

	private Tweet getTweetFromResultSet(ResultSet rs) throws SQLException {
		Tweet t = null;
		try {
			t = new Tweet(
					rs.getLong("tweet_id"),
					this.convertMySQLDateTimeToDate(rs.getString("created_at")),
					rs.getString("screen_name"), rs.getString("text"), rs
							.getDouble("sentiment"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return t;
	}

	public List<Tweet> getBestTweetsForDateRange(Date dateFrom, Date dateTo,
			Index index, int n) throws SQLException {

		Date start = new Date();

		PreparedStatement stmt = connection
				.prepareStatement("SELECT T.tweet_id, screen_name, follower_count, text, created_at, T.sentiment "
						+ "FROM "
						+ DatabaseConfig.getTweetsTableName()
						+ " RIGHT JOIN "
						+ "(SELECT tweet_id, screen_name, follower_count, sentiment "
						+ "FROM "
						+ DatabaseConfig.getTweetsTableName()
						+ " RIGHT JOIN "
						+ DatabaseConfig.getCompaniesTableName()
						+ " ON company_ticker = ticker INNER JOIN "
						+ DatabaseConfig.getTwitterUsersTableName()
						+ " ON twitter_user_id = twitter_id "
						+ "WHERE index_ticker = '"
						+ index.getTicker()
						+ "' "
						+ "AND created_at BETWEEN '"
						+ convertDateToMySQLDateTime(dateFrom)
						+ "' AND '"
						+ convertDateToMySQLDateTime(dateTo)
						+ "' AND follower_count > 100000 "
						+ " AND twitter_id != 216299334 "
						+ // IGNORE PIERS MORGAN
						"ORDER BY follower_count DESC LIMIT "
						+ n
						+ ") AS T ON "
						+ DatabaseConfig.getTweetsTableName()
						+ ".tweet_id = T.tweet_id ORDER BY created_at ASC");

		ResultSet rs = stmt.executeQuery();

		List<Tweet> tweetList = new LinkedList<Tweet>();
		while (rs.next()) {
			Tweet t = new Tweet(rs.getLong("tweet_id"));
			t.setText(rs.getString("text"));
			try {
				t.setCreatedAt(convertMySQLDateTimeToDate(rs
						.getString("created_at")));
			} catch (ParseException e) {
				t.setCreatedAt(dateFrom);
			}
			t.setTwitterUsername(rs.getString("screen_name"));
			t.setSentiment(rs.getDouble("sentiment"));
			tweetList.add(t);
		}

		System.out.println((new Date().getTime() - start.getTime())
				+ " milliseconds to get " + tweetList.size() + " best tweets");
		return tweetList;
	}

}
