package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.util.List;

import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public abstract class SentimentAlgorithm {

	public abstract void analyseTweets(List<Tweet> tweets);

}
