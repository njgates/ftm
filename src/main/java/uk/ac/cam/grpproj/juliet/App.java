package uk.ac.cam.grpproj.juliet;

import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.SQLException;

import uk.ac.cam.grpproj.juliet.db.SentimentAggregateDB;
import uk.ac.cam.grpproj.juliet.ui.AppFrame;
import uk.ac.cam.grpproj.juliet.ui.Dialog;
import uk.ac.cam.grpproj.juliet.ui.Theme;

public class App {
	private Game game;

	public static void main(String[] args) {

		(new Thread() {
			public void run() {
				try {
					// Opening a URL connection to test connection to internet
					URL urlTest = new URL("http://finance.yahoo.com/");
					URLConnection connect = urlTest.openConnection();

					// Allow time for connection
					connect.setConnectTimeout(10000);

					// Get data from source. If data unattainable then no
					// connection
					@SuppressWarnings("unused")
					Object getData = connect.getContent();

				} catch (IOException e1) {
					System.out.println("No Internet Connection");
					Dialog.showError(
							null,
							"An internet connection is required to run this application.",
							"No Internet Connection");
					System.exit(1);
				}
			}
		}).start();

		// If running server via main App
		if (args.length > 0 && args[0].equals("-server")) {
			String[] args2 = new String[args.length - 1];
			for (int i = 0; i < args.length - 1; i++) {
				args2[i] = args[i + 1];
			}
			new Server().runServer(args2);
		} else if (args.length > 0 && args[0].equals("-sentimentAggregate")) {
			try {
				SentimentAggregateDB sDB = new SentimentAggregateDB();
				sDB.recomputeSentimentAggregate();
				sDB.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} else {
			App app = new App();
			app.runApp();
		}
	}

	public void runApp() {
		Theme.setTheme();
		game = new Game(new Settings(), new GameData());
		AppFrame af = new AppFrame(game);
		af.setVisible(true);

		// Put in while loop to force input
		while (game.getUsername() == null || game.getUsername().equals(""))
			game.setUsername(Dialog.showUserInput(af, " Enter a username",
					"Welcome"));
	}
}
