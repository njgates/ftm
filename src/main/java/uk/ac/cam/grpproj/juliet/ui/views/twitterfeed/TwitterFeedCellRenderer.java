package uk.ac.cam.grpproj.juliet.ui.views.twitterfeed;

import java.awt.Color;
import java.awt.Component;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.ListCellRenderer;

import net.miginfocom.swing.MigLayout;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class TwitterFeedCellRenderer extends JPanel implements
		ListCellRenderer<Tweet> {
	private static final long serialVersionUID = -2066156519532651591L;

	private final JTextArea tweetText;
	private final JLabel tweetUser;
	private final JLabel tweetDate;

	public TwitterFeedCellRenderer() {
		this.setLayout(new MigLayout("fill, insets 3, debug"));

		tweetText = new JTextArea();
		tweetText.setLineWrap(true);
		tweetText.setWrapStyleWord(true);
		tweetText.setOpaque(false);

		tweetUser = new JLabel();
		tweetUser.setForeground(Color.black);
		// tweetUser.setBorder(BorderFactory.createLineBorder(Color.black));

		tweetDate = new JLabel();
		tweetDate.setForeground(Color.black);
		// tweetDate.setBorder(BorderFactory.createLineBorder(Color.black));

		this.add(tweetUser, "left");
		this.add(tweetDate, "right, wrap");
		this.add(tweetText, "spanx, grow");
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, Color.black));
	}

	@Override
	public Component getListCellRendererComponent(JList<? extends Tweet> list,
			Tweet value, int index, boolean isSelected, boolean cellHasFocus) {

		tweetText.setText(value.getText());
		tweetUser.setText("Some username");
		tweetDate.setText(value.getCreatedAt().toString());
		tweetUser.setText(value.getTwitterUsername());

		HashMap<Tweet, Float> satMap = ((AnimatedTweetList) list)
				.getSaturationMap();
		float sat = satMap.get(value);

		// Set hue between 0 (red) and 120 (green) degrees
		float hue = (float) ((value.getSentiment() + 1) / 2) * 0.33f;
		Color bg = Color.getHSBColor(hue, sat, 0.8f);
		this.setBackground(bg);

		return this;
	}

}
