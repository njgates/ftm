package uk.ac.cam.grpproj.juliet.ui.views.graph.components;

import java.awt.Dimension;

import javax.swing.JComboBox;

import uk.ac.cam.grpproj.juliet.ui.Theme;
import uk.ac.cam.grpproj.juliet.ui.components.FTMComboBoxRenderer;
import uk.ac.cam.grpproj.juliet.ui.components.FTMComboBoxUI;

public class FTMComboBox<T> extends JComboBox<T> {
	private static final long serialVersionUID = 5221219096852286487L;

	public FTMComboBox() {
		super();
		this.setUI(new FTMComboBoxUI<T>());
		this.setRenderer(new FTMComboBoxRenderer<T>());
	}

	@Override
	public Dimension getPreferredSize() {
		this.setPreferredSize(super.getPreferredSize());
		return new Dimension(super.getPreferredSize().width + 3
				* Theme.borderThickness, super.getPreferredSize().height);
	}
}
