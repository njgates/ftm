package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.io.IOException;
import java.util.List;

import uk.ac.cam.grpproj.juliet.helper.HttpUtility;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;

import com.google.gson.GsonBuilder;

public class Sentiment140 extends SentimentAlgorithm {
	private String apiURL = "http://www.sentiment140.com/api";
	private String apiKey = "njg40@cam.ac.uk";

	@Override
	public void analyseTweets(List<Tweet> tweets) {
		String data = null;
		if (tweets.size() > 10000) {
			int analysed = 0;
			while (analysed < tweets.size()) {
				List<Tweet> toGo = tweets.subList(analysed,
						Math.max(10000, tweets.size() - 1));
				analysed += toGo.size();
				analyseTweets(toGo);
			}
			return;
		} else {
			data = gsonTweets(tweets);
		}

		String requestURL = apiURL + "/bulkClassifyJson?appid=" + apiKey;

		try {
			HttpUtility.sendPostRequest(requestURL, data);
			String response = HttpUtility.readSingleLineResponse();

			// Split string at polarity points
			String regex = "\"polarity\":";
			String[] polarities = response.split(regex);

			// Start at 1 to avoid first part of JSON
			for (int i = 0; i < tweets.size(); i++) {
				String score = polarities[i + 1].substring(0, 1);
				double sentiment = Double.parseDouble(score);

				// How to convert to range -1 to 1?
				// API outputs, 0 negative, 2 neutral and 4 positive
				sentiment = (sentiment / 2) - 1;

				tweets.get(i).setSentiment(sentiment);
			}

		} catch (IOException ex) {
			ex.printStackTrace();
		}
		HttpUtility.disconnect();
	}

	private String gsonTweets(List<Tweet> tweets) {
		// Convert tweets to JSON array
		GsonBuilder gson = new GsonBuilder();
		gson.registerTypeAdapter(Tweet.class, new TweetSerializer());
		String ts = gson.create().toJson(tweets);
		// Wrap in overall data parameter
		String data = "{\"data\": " + ts + "}";
		return data;
	}
}
