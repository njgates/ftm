package uk.ac.cam.grpproj.juliet.ui.views.graph.components;

import java.sql.SQLException;
import java.util.List;

import uk.ac.cam.grpproj.juliet.db.CompanyDB;
import uk.ac.cam.grpproj.juliet.financial.Company;

public class CompanyComboBox extends FTMComboBox<Company> {
	private static final long serialVersionUID = 1158078800430513470L;

	public CompanyComboBox() {
		CompanyDB cdb = null;
		try {
			cdb = new CompanyDB();
			List<Company> comps = cdb.getCompanies();

			// Add indices to indexBox
			for (Company c : comps)
				addItem(c);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		} finally {
			cdb.close();
		}
	}
}
