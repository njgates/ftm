package uk.ac.cam.grpproj.juliet.support;

import java.sql.SQLException;

import uk.ac.cam.grpproj.juliet.db.HistoricalDB;

public class ImportHistoricalData {

	/**
	 * @param args
	 *            args[0] = csvFileLocation args[1] = ticker
	 */
	public static void main(String[] args) {
		HistoricalDB h;
		try {
			h = new HistoricalDB();
			h.importData(args[0], args[1]);
			h.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
