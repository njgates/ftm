package uk.ac.cam.grpproj.juliet.db;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;

import au.com.bytecode.opencsv.CSVReader;

public class HistoricalDB extends DatabaseHelper {

	// TODO remove this class before release

	public HistoricalDB() throws SQLException {
		super();
	}

	public void importData(String csvFile, String ticker) {
		try {
			CSVReader reader = new CSVReader(new FileReader(csvFile));
			String[] nextLine;
			// Insert into ftm_financial_data
			String sql = "INSERT INTO "
					+ DatabaseConfig.getFinancialDataTableName()
					+ " (date,company_ticker,open,high,low,close,volume) VALUES (?,?,?,?,?,?,?)";
			PreparedStatement ps = connection.prepareStatement(sql);
			while ((nextLine = reader.readNext()) != null) {
				// nextLine is array of values from the line
				// Current format of csv is
				// YYYY.MM.DD,HH:MM,open,high,low,close,volume

				// Set date
				ps.setString(1,
						this.convertToDateTime(nextLine[0], nextLine[1]));

				// Set ticker
				ps.setString(2, ticker);

				// Set open
				ps.setDouble(3, Double.parseDouble(nextLine[2]));
				// Set high
				ps.setDouble(4, Double.parseDouble(nextLine[3]));
				// Set low
				ps.setDouble(5, Double.parseDouble(nextLine[4]));
				// Set close
				ps.setDouble(6, Double.parseDouble(nextLine[5]));

				// Set volume
				ps.setDouble(7, Double.parseDouble(nextLine[6]));

				ps.execute();
			}
			ps.close();
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}
}
