package uk.ac.cam.grpproj.juliet.twitter.feed;

import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import uk.ac.cam.grpproj.juliet.db.TwitterDB;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;
import uk.ac.cam.grpproj.juliet.twitter.sentiment.Sentiment140;
import uk.ac.cam.grpproj.juliet.twitter.sentiment.SentimentAnalyser;
import uk.ac.cam.grpproj.juliet.twitter.sentiment.SentimentDelegate;

public class TweetProcessor implements SentimentDelegate {

	private final int MAX_QUEUE = 1000;

	private Date now = new Date();

	private Queue<Tweet> tweetQueue = new LinkedList<Tweet>();

	public void onReceiveTweet(Tweet t) {
		System.out.print("+");
		synchronized (tweetQueue) {
			tweetQueue.add(t);
		}
		if (tweetQueue.size() > MAX_QUEUE) {
			// Reset counter
			System.out.println(MAX_QUEUE + " tweets in "
					+ ((new Date().getTime()) - now.getTime()) / 1000 + " s");
			// Fire off sentiment analysis
			analyseSentiment();
		}
	}

	private void analyseSentiment() {
		List<Tweet> tweets = new LinkedList<Tweet>();
		synchronized (tweetQueue) {
			for (int i = 0; i < MAX_QUEUE; i++)
				tweets.add(tweetQueue.remove());
		}
		SentimentAnalyser sa = new SentimentAnalyser(new Sentiment140(), this);
		System.out.println("Starting sentiment analysis on " + tweets.size()
				+ "tweets.");
		sa.analyseTweets(tweets);
	}

	@Override
	public void didFinishSentimentAnalysis(List<Tweet> tweets) {
		// Put tweets in database
		TwitterDB tdb = null;
		try {
			tdb = new TwitterDB();
			System.out.println("Finished sentiment analysis, inserting tweets");
			tdb.insertBulkTweets(tweets);
		} catch (SQLException e) {
			System.err.println("Sentiment Analysis Done: " + e.getMessage());
		} finally {
			tdb.close();
			tweets = null;
		}
	}

}
