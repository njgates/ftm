package uk.ac.cam.grpproj.juliet.ui.components;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;
import javax.swing.MenuElement;
import javax.swing.MenuSelectionManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicComboBoxUI;
import javax.swing.plaf.basic.BasicComboPopup;
import javax.swing.plaf.basic.ComboPopup;

import uk.ac.cam.grpproj.juliet.ui.Theme;

public class FTMComboBoxUI<T> extends BasicComboBoxUI {

	public static ComponentUI createUI(JComponent c) {
		return new FTMComboBoxUI();
	}

	@Override
	protected JButton createArrowButton() {
		return new JButton() {
			private static final long serialVersionUID = -8360847882831994537L;

			@Override
			public int getWidth() {
				return 0;
			}
		};
	}

	@Override
	protected ComboPopup createPopup() {
		return new FTMComboPopup(this.comboBox);
	}

	private class FTMComboPopup extends BasicComboPopup {
		private static final long serialVersionUID = 1389744017891652801L;

		public FTMComboPopup(JComboBox<T> comboBox) {
			super(comboBox);
		}

		@Override
		protected void configureScroller() {
			// Hide scrollbar
			this.scroller.setFocusable(false);
			this.scroller.getVerticalScrollBar().setFocusable(false);
			this.scroller.setBorder(null);
			this.scroller.setOpaque(false);
		}

		@Override
		protected void configurePopup() {
			setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
			setBorderPainted(true);
			// setBorder(Theme.frameBorder);
			setOpaque(false);
			// Why if it's hidden?
			add(this.scroller);
			setDoubleBuffered(true);
			setFocusable(false);
		}

		@Override
		protected void configureList() {
			super.configureList();
			this.list.setBackground(Theme.comboBG);
		}

		@Override
		public void hide() {
			MenuSelectionManager manager = MenuSelectionManager
					.defaultManager();
			MenuElement[] selection = manager.getSelectedPath();
			for (MenuElement element : selection) {
				if (element == this) {
					manager.clearSelectedPath();
					break;
				}
			}
			this.comboBox.repaint();
		}

		@Override
		public void show() {
			setListSelection(this.comboBox.getSelectedIndex());
			Point location = getPopupLocation();
			show(this.comboBox, location.x, location.y);
		}

		private void setListSelection(int selectedIndex) {
			if (selectedIndex == -1) {
				this.list.clearSelection();
			} else {
				this.list.setSelectedIndex(selectedIndex);
				this.list.ensureIndexIsVisible(selectedIndex);
			}
		}

		private Point getPopupLocation() {
			Dimension popupSize = new Dimension((int) this.comboBox.getSize()
					.getWidth(), (int) this.comboBox.getSize().getHeight());
			Insets insets = getInsets();
			popupSize.setSize(popupSize.width - (insets.right + insets.left),
					getPopupHeightForRowCount(this.comboBox
							.getMaximumRowCount()));
			Rectangle popupBounds = computePopupBounds(0,
					this.comboBox.getBounds().height, popupSize.width,
					popupSize.height);
			Dimension scrollSize = popupBounds.getSize();
			Point popupLocation = popupBounds.getLocation();

			this.scroller.setMaximumSize(scrollSize);
			this.scroller.setPreferredSize(scrollSize);
			this.scroller.setMinimumSize(scrollSize);

			this.list.revalidate();
			return popupLocation;
		}
	}

	public void paintCurrentValue(Graphics g, Rectangle bounds, boolean hasFocus) {
		ListCellRenderer<T> renderer = this.comboBox.getRenderer();

		Component c;

		if (hasFocus && !isPopupVisible(this.comboBox)) {
			c = renderer.getListCellRendererComponent(this.listBox,
					(T) this.comboBox.getSelectedItem(), -1, true, false);
		} else {
			c = renderer.getListCellRendererComponent(this.listBox,
					(T) this.comboBox.getSelectedItem(), -1, false, false);
		}

		boolean shouldValidate = false;
		if (c instanceof JPanel) {
			shouldValidate = true;
		}

		c.setBackground(new Color(255, 255, 255, 0));

		this.currentValuePane.paintComponent(g, c, this.comboBox, bounds.x,
				bounds.y, bounds.width, bounds.height, shouldValidate);
	}
}
