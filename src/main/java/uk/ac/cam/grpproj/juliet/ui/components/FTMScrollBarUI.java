package uk.ac.cam.grpproj.juliet.ui.components;

import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.RenderingHints;

import javax.swing.JComponent;
import javax.swing.JScrollBar;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicScrollBarUI;

import uk.ac.cam.grpproj.juliet.ui.Theme;

public class FTMScrollBarUI extends BasicScrollBarUI {

	public static ComponentUI createUI(JComponent c) {
		return new FTMScrollBarUI();
	}

	@Override
	protected void paintThumb(Graphics g, JComponent c, Rectangle trackBounds) {
		Graphics2D g2 = (Graphics2D) g.create();
		g2.setPaint((Paint) Color.red);
		g2.drawRoundRect(trackBounds.x, trackBounds.y, trackBounds.width,
				trackBounds.height, Theme.borderRadius, Theme.borderRadius);
	}

	@Override
	protected void paintTrack(Graphics g, JComponent c, Rectangle trackBounds) {

		JScrollBar b = (JScrollBar) c;
		b.setBackground(Theme.bgColor2);

		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		// Create Background
		g2.setPaint((Paint) Theme.bgColor2);
		g2.drawRect(trackBounds.x, trackBounds.y, trackBounds.width,
				trackBounds.height);

		// Create gradient BG
		g2.setPaint(new GradientPaint(new Point(0, 0), Theme.topGradient,
				new Point(0, c.getHeight()), Theme.bottomGradient));
		g2.fillRoundRect(Theme.borderThickness, Theme.borderThickness,
				c.getWidth() - (2 * Theme.borderThickness), c.getHeight()
						- (2 * Theme.borderThickness), Theme.borderRadius
						- Theme.borderThickness, Theme.borderRadius
						- Theme.borderThickness);

		// Create highlight
		g2.setPaint((Paint) Theme.highlight);
		g2.drawLine(1 + Theme.borderThickness, Theme.borderThickness,
				c.getWidth() - (2 * Theme.borderThickness),
				Theme.borderThickness);

		g2.dispose();
	}

}
