package uk.ac.cam.grpproj.juliet.ui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import uk.ac.cam.grpproj.juliet.DataPoints;
import uk.ac.cam.grpproj.juliet.Game;
import uk.ac.cam.grpproj.juliet.GameData;
import uk.ac.cam.grpproj.juliet.GameState;
import uk.ac.cam.grpproj.juliet.Settings;
import uk.ac.cam.grpproj.juliet.db.TwitterDB;
import uk.ac.cam.grpproj.juliet.db.YahooDB;
import uk.ac.cam.grpproj.juliet.financial.Company;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;
import uk.ac.cam.grpproj.juliet.twitter.sentiment.ProcessSentiment;
import uk.ac.cam.grpproj.juliet.ui.views.graph.Graph;
import uk.ac.cam.grpproj.juliet.ui.views.instructions.InstructionsView;
import uk.ac.cam.grpproj.juliet.ui.views.playback.PlaybackControls;
import uk.ac.cam.grpproj.juliet.ui.views.playback.PlaybackDelegate;
import uk.ac.cam.grpproj.juliet.ui.views.statistics.Statistics;
import uk.ac.cam.grpproj.juliet.ui.views.twitterfeed.TwitterFeed;

//import uk.ac.cam.grpproj.juliet.ui.views.ticker.Ticker;

public class AppFrame extends JFrame implements PlaybackDelegate {

	private static final long serialVersionUID = 1620441903992642015L;
	private Game game;
	public GameState state = GameState.START;

	JButton restartButton = new JButton("Restart");
	JButton hsButton = new JButton("Highscores");
	JButton submitButton = new JButton("Submit");

	JPanel twitterFeed;
	Graph graph;
	Statistics stats;
	PlaybackControls pb;

	// Playback properties
	private Timer timer = new Timer();
	private List<PlaybackPanel> playbackPanels = new LinkedList<PlaybackPanel>();
	private boolean paused = false;
	private Date currentDate;
	// Seconds to move forward per tick
	private int speed = 60 * 60 * 1; // 1 hour
	private long delay = 1000;
	private long period = 100;

	public AppFrame(final Game game) {
		// Default size and structure
		setResizable(true);
		setTitle("Game");
		this.game = game;

		// Action upon closing
		setDefaultCloseOperation(EXIT_ON_CLOSE);

		// // Initial layout panel
		JPanel jp = new JPanel();
		jp.setLayout(new MigLayout("insets 3", "[][grow][]", "[grow][][]"));
		getContentPane().add(jp, BorderLayout.CENTER);

		// Twitter Feed
		twitterFeed = new JPanel();
		twitterFeed.setLayout(new MigLayout("insets 0, fill"));
		twitterFeed.setBorder(BorderFactory.createLineBorder(
				Theme.borderGradTop, Theme.borderThickness, true));
		updateTwitterFeed();
		jp.add(twitterFeed, "cell 0 0 0 1, width 355:355:null, grow");

		// Add graph
		graph = new Graph(this, game);
		graph.update();
		jp.add(graph, "cell 1 0 2 0, grow");
		playbackPanels.add(graph);

		// Add stats
		stats = new Statistics(game.getGameData());
		stats.setVisible(false);
		jp.add(stats, "cell 0 1, grow");
		playbackPanels.add(stats);

		// Add Playback
		pb = new PlaybackControls(this, speed);
		pb.setVisible(false);
		jp.add(pb, "cell 2 1");
		playbackPanels.add(pb);

		// Add buttons
		jp.add(hsButton, "cell 0 2");
		hsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				HSFrame.getInstance().setVisible(true);
			}
		});

		// Add ticker - MAYBE
		// Ticker ticker = new Ticker("Some string", Theme.textColor,
		// Theme.bgColor);
		// jp.add(ticker, "cell 0 2, spanx 3, grow");
		// ticker.start();

		restartButton.setEnabled(false);
		jp.add(restartButton, "cell 2 2, tag cancel");
		restartButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				switch (state) {
				case INPUT:
					for (int i = game.getGameData().getMarketPredicted()
							.getItemCount() - 1; i > 0; i--)
						game.getGameData().getMarketPredicted().remove(i);
					break;
				case DATA:
					// TODO clear everything and revert to START/INPUT?
					state = GameState.INPUT;
					AppFrame.this.stateChanged();
				default:
					break;
				}
			}
		});

		submitButton.setEnabled(false);
		jp.add(submitButton, "cell 2 2, tag ok");
		submitButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent ae) {
				if (game.getGameData().getMarketPredicted().getItemCount() <= 1) {
					Dialog.showWarning(AppFrame.this,
							"You must enter at least one predicted point.",
							"Not enough points!");
				} else {
					state = GameState.DATA;
					AppFrame.this.stateChanged();
				}
			}
		});

		this.pack();
		setLocationRelativeTo(null);
	}

	public void stateChanged() {
		switch (state) {
		case START:
			System.out.println("State: START");
			break;
		case INPUT:
			System.out.println("State: INPUT");
			// Get sentiment data
			YahooDB ydb = null;
			GameData d = game.getGameData();
			Settings s = game.getSettings();
			try {
				ydb = new YahooDB();
				int period = this.getTimeFrameForInterval(s.getDateTo()
						.getTime() - s.getDateFrom().getTime());
				System.out.println("Getting data for " + period
						+ "m time period.");
				d.setMarketActual(ydb.getAggregatedFinancialData(new Company(s
						.getIndex().getTicker()), DataPoints.getIntervalStart(s
						.getDateFrom()), DataPoints.getIntervalEnd(s
						.getDateTo()), period));
				System.out.println("Processing sentiment.");
				d.setSentiment(ProcessSentiment.getSentimentSeries(
						d.getMarketActual(), s.getIndex()));
				System.out.println("Done.");
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (DataPoints.IllegalIntervalException e) {
				e.printStackTrace();
			} finally {
				ydb.close();
			}
			
			break;
		case DATA:
			System.out.println("State: DATA");
			break;
		}
		updateTwitterFeed();
		updateStats();
		updatePlayback();
		updateButtons();
		graph.update();
		validate();
	}

	private void updateButtons() {
		switch (state) {
		case START:
			submitButton.setVisible(true);
			submitButton.setEnabled(false);
			restartButton.setEnabled(false);
			break;
		case INPUT:
			submitButton.setVisible(true);
			submitButton.setEnabled(true);
			restartButton.setEnabled(true);
			break;
		case DATA:
			submitButton.setVisible(false);
			stats.setVisible(true);
			break;
		default:
			stats.setVisible(false);
			break;
		}
	}

	private void updateStats() {
		switch (state) {
		case DATA:
			stats.setScore(game.getScore());
			stats.setVisible(true);
			break;
		default:
			stats.setVisible(false);
			break;
		}
	}

	private void updatePlayback() {
		switch (state) {
		case DATA:
			pb.setVisible(true);
			break;
		default:
			pb.setVisible(false);
			break;
		}
	}

	public void updateTwitterFeed() {
		twitterFeed.removeAll();
		switch (state) {
		case DATA:
			TwitterDB tdb = null;
			List<Tweet> ts;
			try {
				tdb = new TwitterDB();
				ts = tdb.getBestTweetsForDateRange(game.getSettings()
						.getDateFrom(), game.getSettings().getDateTo(), game
						.getSettings().getIndex(), 100);
			} catch (SQLException e) {
				// Leave blank
				System.err.println(e.getMessage());
				ts = new LinkedList<Tweet>();
			} finally {
				tdb.close();
			}
			TwitterFeed tf = new TwitterFeed(ts);
			playbackPanels.add(tf);
			twitterFeed.add(tf, "grow");
			break;
		default:
			// Instructions
			twitterFeed.add(new InstructionsView(), "grow");
			break;
		}
	}

	private int getTimeFrameForInterval(long interval) {
		// Put into hours
		interval /= 60000;
		interval /= 60;

		// If a day, then 15 minute
		if (interval <= 24)
			return 15;

		// If 3 days, then half hour
		if (interval <= 24 * 3)
			return 30;

		// If week, then hour
		if (interval <= 24 * 7)
			return 60;

		// If month, then 4 hours
		if (interval <= 24 * 7 * 4)
			return (60 * 4);

		// Else return daily
		return (60 * 24);
	}

	@Override
	public void didPlay() {
		currentDate = game.getSettings().getDateFrom();
		for (PlaybackPanel p : playbackPanels) {
			if (paused)
				p.playbackResumed();
			else
				p.playbackStarted();
		}
		paused = false;

		timer = new Timer();
		timer.schedule(new updateViews(), delay, period);
	}

	@Override
	public void didStop() {
		paused = false;
		timer.cancel();
		for (PlaybackPanel p : playbackPanels)
			p.playbackStopped();
		currentDate = game.getSettings().getDateFrom();
	}

	@Override
	public void didPause() {
		paused = true;
		timer.cancel();
	}

	@Override
	public void didChangeSpeed(int speed) {
		this.speed = speed;
	}

	private class updateViews extends TimerTask {
		@Override
		public void run() {
			currentDate = new Date(currentDate.getTime() + 1000 * speed);
			if (currentDate.after(game.getSettings().getDateTo())) {
				AppFrame.this.didStop();
				return;
			}
			for (PlaybackPanel p : playbackPanels)
				p.updatePanelToDate(new Date(currentDate.getTime()));
		}
	}
}
