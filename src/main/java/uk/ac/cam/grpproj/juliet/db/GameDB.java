package uk.ac.cam.grpproj.juliet.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import uk.ac.cam.grpproj.juliet.Game;
import uk.ac.cam.grpproj.juliet.GameData;
import uk.ac.cam.grpproj.juliet.Settings;

public class GameDB extends DatabaseHelper {

	public GameDB() throws SQLException {
		super();
	}

	// Returns top n highscores from database as Game objects
	public List<Game> getHighScores(int n) throws SQLException, ParseException {
		String sql = "SELECT game_id, username, created_at, score FROM "
				+ DatabaseConfig.getGameDataTableName()
				+ " ORDER BY score DESC LIMIT " + n;
		ResultSet results = connection.createStatement().executeQuery(sql);

		List<Game> games = new LinkedList<Game>();
		while (results.next()) {
			// Do date
			Date createdAt = this.convertMySQLDateTimeToDate(results
					.getString("created_at"));
			games.add(new Game(results.getInt("game_id"), results
					.getString("username"), createdAt, results
					.getDouble("score")));
		}
		return games;
	}

	public Settings getSettingsForGame(int id) throws SQLException {
		String sql = "SELECT settings FROM "
				+ DatabaseConfig.getGameDataTableName() + " WHERE game_id=\""
				+ id + "\"";
		ResultSet results = connection.createStatement().executeQuery(sql);

		results.first();
		Settings s = (Settings) results.getObject("settings");

		if (s == null) {
			System.err.println("Could not get settings");
		}

		return s;
	}

	public GameData getGameDataForGame(int id) throws SQLException {
		String sql = "SELECT settings FROM "
				+ DatabaseConfig.getGameDataTableName() + " WHERE game_id=\""
				+ id + "\"";
		ResultSet results = connection.createStatement().executeQuery(sql);

		results.first();
		GameData g = (GameData) results.getObject("game_data");

		if (g == null) {
			System.err.println("Could not get game data");
		}

		return g;
	}

}
