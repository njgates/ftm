package uk.ac.cam.grpproj.juliet.twitter.feed;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import uk.ac.cam.grpproj.juliet.db.CompanyDB;
import uk.ac.cam.grpproj.juliet.financial.Company;
import uk.ac.cam.grpproj.juliet.financial.Index;

public class TwitterKeywords {

	public static Map<String, Company> getKeywordsForIndex(Index index)
			throws SQLException {
		CompanyDB cdb = new CompanyDB();
		List<Company> companies = cdb.getCompaniesForIndex(index.getTicker());
		cdb.close();

		Map<String, Company> tags = new HashMap<String, Company>();
		for (Company c : companies) {
			for (String s : getKeywordsForCompany(c)) {
				tags.put(s, c);
			}
		}

		return tags;
	}

	public static List<String> getKeywordsForCompany(Company company) {
		List<String> tags = new LinkedList<String>();
		// Name
		String compName = normaliseCompanyName(company.getName());
		if (!WordList.getInstance().isWordEnglish(compName))
			tags.add(normaliseCompanyName(company.getName()));
		else
			System.out.println("Company name skipped: "
					+ normaliseCompanyName(company.getName()));

		// Ticker
		String ticker = company.getTicker().replaceAll("^\\^", "");
		// Remove any english word ticker such as "next" and "life"
		if (!WordList.getInstance().isWordEnglish(ticker)) {
			tags.add(company.getTicker().replaceAll("^\\^", ""));
			tags.add("$" + company.getTicker().replaceAll("\\.L", ""));
		} else {
			System.out.println("Ticker skipped: " + company.getTicker()
					+ " for " + normaliseCompanyName(company.getName()));
		}

		return tags;
	}

	private static String normaliseCompanyName(String name) {
		// Change full company name into something normal people would use
		// in their tweets.
		name = name.replaceAll("(, )?(Inc\\.?|Incorporated)$", "").trim();
		name = name.replaceAll("(, )?(Corp\\.?|Corporation)$", "").trim();
		name = name.replaceAll("(, )?(Ltd\\.?|Limited)$", "").trim();
		name = name.replaceAll("(, )?International$", "").trim();
		name = name.replaceAll("\\.com$", "").trim();
		return name;
	}
}
