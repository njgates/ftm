package uk.ac.cam.grpproj.juliet;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import uk.ac.cam.grpproj.juliet.db.GameDB;
import uk.ac.cam.grpproj.juliet.helper.Score;

public class Game implements Serializable {
	private static final long serialVersionUID = 2980979335545371641L;

	private int id;
	private String username;
	private Date created_at;
	private Settings settings = null;
	private GameData game_data = null;
	private Double score;

	/* To be used when first creating a game in the client */
	public Game(Settings settings, GameData gameData) {
		this.settings = settings;
		this.game_data = gameData;
		this.created_at = new Date();
		this.score = null;
	}

	/*
	 * To be used when loading game from database Don't load Settings or
	 * GameData since they should be retrieve on request from DB
	 */
	public Game(int id, String username, Date created_at, Double score) {
		this.id = id;
		this.username = username;
		this.created_at = created_at;
		this.score = score;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getId() {
		return id;
	}

	public Date getCreatedAt() {
		return created_at;
	}

	public Settings getSettings() {
		if (settings == null) {
			try {
				GameDB g = new GameDB();
				settings = g.getSettingsForGame(this.id);
				g.close();
			} catch (SQLException e) {
				settings = new Settings();
			}
		}
		return settings;
	}

	public GameData getGameData() {
		if (game_data == null) {
			try {
				GameDB g = new GameDB();
				game_data = g.getGameDataForGame(this.id);
				g.close();
			} catch (SQLException e) {
				game_data = new GameData();
			}
		}
		return game_data;
	}

	public Double getScore() {
		// if (score == null) {
		Score s = new Score(game_data.getMarketActual(),
				game_data.getMarketPredicted());
		score = s.getScore();
		// }
		return score;
	}
}
