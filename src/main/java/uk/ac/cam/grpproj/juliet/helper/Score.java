package uk.ac.cam.grpproj.juliet.helper;

import java.util.LinkedList;
import java.util.List;

import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;

public class Score {

	private final XYSeries actual;
	private final XYSeries pred;

	public Score(XYSeries actual, XYSeries pred) {
		this.actual = actual;
		this.pred = pred;
	}

	public double getScore() {
		double totalScore = 0.0;
		for (int i = 0; i < pred.getItemCount() - 1; i++) {
			totalScore += calculateSegment(i);
		}
		double missedItemScaling = ((double) (actual.getItemCount() - missedItems()))
				/ ((double) actual.getItemCount());
		return (totalScore / 1000) / missedItemScaling;
	}

	private double calculateSegment(int segment) {
		double c = pred.getY(segment).doubleValue();
		double dy = pred.getY(segment + 1).doubleValue()
				- pred.getY(segment).doubleValue();
		double dx = pred.getX(segment + 1).doubleValue()
				- pred.getX(segment).doubleValue();
		double m = dy / dx;

		List<Double> diffSquared = new LinkedList<Double>();
		List<XYDataItem> itemsInSegment = new LinkedList<XYDataItem>();
		int i = 0;
		// Get left index of segment for actual
		while (actual.getX(i).doubleValue() < pred.getX(segment).doubleValue()
				&& (i < actual.getItemCount() - 1)) {
			i++;
		}
		try {
			// Get list of items
			while ((actual.getX(i).doubleValue() < pred.getX(segment + 1)
					.doubleValue()) && (i < actual.getItemCount() - 1)) {
				itemsInSegment.add((actual.getDataItem(i)));
				i++;

			}
		} catch (IndexOutOfBoundsException e) {
			// Ignore
		}

		// Get differences
		double score = 0.0;
		if (i > 0) {
			double x0Val = pred.getX(segment).doubleValue();
			// double xHVal = pred.getX(segment+1).doubleValue();
			for (XYDataItem item : itemsInSegment) {
				// interpolate predVal
				double predVal = (m * (item.getXValue() - x0Val)) + c;
				double diff = item.getYValue() - predVal;
				diffSquared.add(new Double(sq(diff)));
			}

			double sum = 0.0;
			for (double d : diffSquared) {
				sum += d;
			}

			// to normalise for the range of the graph
			score = sum
					/ Math.abs((actual.getY(actual.getItemCount() - 1)
							.doubleValue() - actual.getY(0).doubleValue()));
		}
		return score;
	}

	private double sq(double x) {
		return (x * x);
	}

	private int missedItems() {
		int i = 0;
		while (actual.getX(i).doubleValue() < pred
				.getX(pred.getItemCount() - 1).doubleValue()
				&& (i < actual.getItemCount() - 1)) {
			i++;
		}
		int j = 0;
		while (actual.getX(i).doubleValue() < actual.getX(
				actual.getItemCount() - 1).doubleValue()) {
			i++;
			j++;
		}
		return j;
	}
}
