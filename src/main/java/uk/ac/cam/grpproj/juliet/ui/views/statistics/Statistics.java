package uk.ac.cam.grpproj.juliet.ui.views.statistics;

import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import uk.ac.cam.grpproj.juliet.GameData;
import uk.ac.cam.grpproj.juliet.ui.PlaybackPanel;

public class Statistics extends JPanel implements PlaybackPanel {
	private static final long serialVersionUID = -8784019467356736040L;

	private GameData data;
	private JLabel score;

	public Statistics(GameData data) {
		this.data = data;
		this.setLayout(new MigLayout());
		score = new JLabel();
		add(score);
	}

	@Override
	public void updatePanelToDate(Date date) {
		// TODO Auto-generated method stub

	}

	@Override
	public void playbackStarted() {
		// TODO Auto-generated method stub

	}

	@Override
	public void playbackStopped() {
		// TODO Auto-generated method stub

	}

	@Override
	public void playbackResumed() {
		// TODO Auto-generated method stub

	}

	public void setScore(Double score) {
		this.score.setText("Score: " + score);
	}

}
