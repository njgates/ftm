package uk.ac.cam.grpproj.juliet.ui;

import java.awt.BorderLayout;
import java.sql.SQLException;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;

import net.miginfocom.swing.MigLayout;
import uk.ac.cam.grpproj.juliet.db.CompanyDB;
import uk.ac.cam.grpproj.juliet.financial.Index;

public final class HSFrame extends JFrame {
	private static final long serialVersionUID = 1620441903992642015L;

	private static HSFrame self;

	public static HSFrame getInstance() {
		if (self == null)
			self = new HSFrame();
		return self;
	}

	public HSFrame() {
		// Manages layout of frame
		setTitle("Highscores");
		setSize(300, 500);
		setResizable(false);
		setLocationRelativeTo(null);

		// Basic layout panel
		JPanel jp = new JPanel();
		jp.setLayout(new MigLayout("fill, nogrid"));
		add(jp, BorderLayout.CENTER);

		jp.add(new JTable(), "grow");

		// Tabs for each mode
		JTabbedPane jtp = new JTabbedPane();
		CompanyDB cdb = null;
		try {
			cdb = new CompanyDB();
			List<Index> indices = cdb.getIndices();

			// Make an array of index tables to associate a table with each
			// index
			int indexListSize = indices.size();
			JTable[] indexHS = new JTable[indexListSize];

			// Test data - replace rowData with getName, getScores, etc and
			// place within 'for' statement
			String[] columnHeadings = { "Name", "Score", "etc" };
			Object[][] rowData = { { "ExampleName1", "ExampleScore1", "etc" },
					{ "ExampleName2", "ExampleScore2", "..." } };

			for (Index i : indices) {
				int tabNumber = 0;

				// New table with above data
				JTable indexTable = new JTable(rowData, columnHeadings);
				indexHS[tabNumber] = indexTable;

				JScrollPane jsp = new JScrollPane(indexHS[tabNumber]); // Needed
																		// to
																		// see
																		// headings
				jtp.addTab(i.toString(), jsp);

				tabNumber++; // To get next index table
			}
		} catch (SQLException e1) {
			// Some error
			// TODO show an error box
		} finally {
			cdb.close();
		}
		jp.add(jtp, "cell 0 0, grow");
	}

}
