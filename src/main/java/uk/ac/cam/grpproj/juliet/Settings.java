package uk.ac.cam.grpproj.juliet;

import java.io.Serializable;
import java.util.Date;

import uk.ac.cam.grpproj.juliet.financial.Index;

public class Settings implements Serializable {

	private static final long serialVersionUID = 439749773355541272L;

	private Index index = null;
	private Date dateFrom = null;
	private Date dateTo = null;
	private GameMode mode = null;

	public Index getIndex() {
		return index;
	}

	public void setIndex(Index index) {
		this.index = index;
	}

	public Date getDateFrom() {
		return dateFrom;
	}

	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	public Date getDateTo() {
		return dateTo;
	}

	public void setDateTo(Date dateTo) {
		this.dateTo = dateTo;
	}

	public GameMode getMode() {
		return mode;
	}

	public void setMode(GameMode mode) {
		this.mode = mode;
	}
}