package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.util.Date;

public class SentimentAggregate {
	private String company_ticker;
	private Date timestamp;
	private int sentiment_positive;
	private int sentiment_neutral;
	private int sentiment_negative;

	private final double WEIGHT_POS = 1.0;
	private final double WEIGHT_NEU = -0.03;
	private final double WEIGHT_NEG = -2.0;

	public SentimentAggregate(String company_ticker, Date timestamp,
			int sentiment_positive, int sentiment_neutral,
			int sentiment_negative) {
		this.company_ticker = company_ticker;
		this.timestamp = timestamp;
		this.sentiment_positive = sentiment_positive;
		this.sentiment_neutral = sentiment_neutral;
		this.sentiment_negative = sentiment_negative;
	}

	public String getTicker() {
		return this.company_ticker;
	}

	public Date getTimestamp() {
		return this.timestamp;
	}

	public int getSentimentPositive() {
		return this.sentiment_positive;
	}

	public int getSentimentNeutral() {
		return this.sentiment_neutral;
	}

	public int getSentimentNegative() {
		return this.sentiment_negative;
	}

	public double getSentiment() {
		 double total = (this.sentiment_positive + this.sentiment_neutral +
		 this.sentiment_negative);		
		 
		  return (this.sentiment_positive * WEIGHT_POS + this.sentiment_neutral
		  * WEIGHT_NEU + this.sentiment_negative * WEIGHT_NEG) / total;
		 

		//if (this.sentiment_negative == 0)
			//this.sentiment_negative++;

		//return Math.log(this.sentiment_positive / this.sentiment_negative);
	}

	public SentimentAggregate add(SentimentAggregate other) {
		if (!company_ticker.equals(other.getTicker())) {
			return null;
		}
		if (timestamp.compareTo(other.getTimestamp()) > 0) {
			return null;
		}
		return new SentimentAggregate(company_ticker, timestamp,
				sentiment_positive + other.getSentimentPositive(),
				sentiment_neutral + other.getSentimentNeutral(),
				sentiment_negative + other.getSentimentNegative());
	}
}
