package uk.ac.cam.grpproj.juliet.financial;

import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import uk.ac.cam.grpproj.juliet.db.CompanyDB;
import uk.ac.cam.grpproj.juliet.db.YahooDB;

public class YahooDataFetcher extends TimerTask {

	private final int interval;

	public YahooDataFetcher(int intervalInMins) {
		this.interval = intervalInMins;
	}

	public void startYahooDataFetcher() {
		long timeMs = System.currentTimeMillis();
		long delay = (interval * 60 * 1000) - (timeMs % (interval * 60 * 1000));
		Timer t = new Timer();
		t.scheduleAtFixedRate(this, delay, interval * 60 * 1000);
	}

	public void run() {
		try {
			YahooDB ydb = new YahooDB();
			CompanyDB cdb = new CompanyDB();
			YahooAPI yapi = new YahooAPI();
			ydb.writeFinancialData(yapi.getYahooData(cdb.getCompanies()));
			System.out.println("Collected data for: " + new Date().toString());
			cdb.close();
			ydb.close();
		} catch (MalformedURLException e) {
			System.err.println("Financial Data URL is malformed.");
			return;
		} catch (SQLException e) {
			System.err.println("SQL error. " + e.getMessage());
			return;
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return;
		}
	}
}
