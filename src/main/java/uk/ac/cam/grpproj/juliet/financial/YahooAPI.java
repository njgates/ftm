package uk.ac.cam.grpproj.juliet.financial;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class YahooAPI {

	public YahooAPI() {
		super();
	}

	// takes a List of Companies and returns a List of Prices for those
	// Companies, using the Yahoo API
	public List<Price> getYahooData(List<Company> companyList)
			throws MalformedURLException, SQLException, IOException {
		return returnListOfPrices(getCSVasList(splitInto100s(companyList)));
	}

	// splits a List of Companies into a List of Strings (max length: 100) of
	// company ticker names
	public List<String> splitInto100s(List<Company> companyList) {
		List<String> companyList100 = new LinkedList<String>();
		int i = 0;
		int j = 0;
		while (j < companyList.size()) {
			String s = "";
			String t;
			while (i < 100 && j < companyList.size()) {
				s += "+";
				t = companyList.get(j).getTicker();
				if (t.startsWith("^")) {
					t.substring(1);
					t = "%5E" + t;
				}
				s += companyList.get(j).getTicker();
				i++;
				j++;
			}
			companyList100.add(s.substring(1));
			i = 0;
		}
		return companyList100;
	}

	// returns a List of Strings (containing Yahoo data on each company) given
	// an input List of Strings (max length: 100) of company ticker names
	public List<String> getCSVasList(List<String> requestList)
			throws IOException, MalformedURLException {
		List<String> returnList = new LinkedList<String>();
		int i;
		for (i = 0; i < requestList.size(); ++i) {
			URL url = new URL("http://finance.yahoo.com/d/quotes.csv?s="
					+ requestList.get(i) + "&f=sl1&e=.csv");
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					url.openStream()));
			String s = null;
			while ((s = reader.readLine()) != null) {
				returnList.add(s);
			}
		}
		return returnList;
	}

	// returns a List of Prices given a List of Strings (containing Yahoo data
	// on each company)
	public List<Price> returnListOfPrices(List<String> dataList)
			throws SQLException {
		List<Price> returnList = new LinkedList<Price>();
		Date date = new Date();
		for (String s : dataList) {
			String[] cols = new String[2];
			cols = s.split(",");
			if (!cols[1].equals("N/A")) {
				String ticker = cols[0].replaceAll("^\"|\"$", "");
				Price p = new Price(ticker, Double.parseDouble(cols[1]), date);
				returnList.add(p);
			}
		}
		return returnList;
	}

}
