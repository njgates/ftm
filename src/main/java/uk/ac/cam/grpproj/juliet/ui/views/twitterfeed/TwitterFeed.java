package uk.ac.cam.grpproj.juliet.ui.views.twitterfeed;

import java.awt.BorderLayout;
import java.util.Date;
import java.util.List;

import javax.swing.DefaultListModel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import uk.ac.cam.grpproj.juliet.twitter.Tweet;
import uk.ac.cam.grpproj.juliet.ui.PlaybackPanel;
import uk.ac.cam.grpproj.juliet.ui.Theme;

public class TwitterFeed extends JPanel implements PlaybackPanel {
	private static final long serialVersionUID = -3616888229105689061L;

	private DefaultListModel<Tweet> model;
	private AnimatedTweetList list;
	private final List<Tweet> tweets;

	private int tweetPlaybackIndex = 0;

	public TwitterFeed(List<Tweet> tweets) {
		this.setLayout(new BorderLayout());
		this.setBackground(Theme.comboBG);
		model = new DefaultListModel<Tweet>();
		list = new AnimatedTweetList(model);
		this.tweets = tweets;

		JScrollPane scroll = new JScrollPane(list);
		scroll.setBackground(Theme.bgColor);
		// Smoother scrolling
		scroll.getVerticalScrollBar().setUnitIncrement(10);

		add(scroll, BorderLayout.CENTER);

		this.loadTweets();
	}

	public void loadTweets() {
		model.clear();
		for (Tweet t : tweets)
			this.addTweet(t);
	}

	public void addTweet(Tweet t) {
		// Insert tweet at top of list
		model.insertElementAt(t, 0);
	}

	@Override
	public void updatePanelToDate(Date date) {
		if (tweets.size() > 0) {
			// Check for zero items
			if (model.size() == 0) {
				model.add(0, tweets.get(0));
				tweetPlaybackIndex++;
			}

			// Get date of latest item added
			Date latest = model.get(0).getCreatedAt();

			// Add all items up to the date given
			while (latest.before(date) && tweetPlaybackIndex < tweets.size()) {
				Tweet t = tweets.get(tweetPlaybackIndex);
				latest = t.getCreatedAt();
				if (latest.before(date)) {
					this.addTweet(t);
					tweetPlaybackIndex++;
				}
			}
		}
	}

	@Override
	public void playbackStarted() {
		// Empty list
		tweetPlaybackIndex = 0;
		model.clear();
	}

	@Override
	public void playbackStopped() {
		// Empty list
		tweetPlaybackIndex = 0;
		// Load all tweets
		this.loadTweets();
	}

	@Override
	public void playbackResumed() {
		// updatePanelToDate should handle this
	}
}
