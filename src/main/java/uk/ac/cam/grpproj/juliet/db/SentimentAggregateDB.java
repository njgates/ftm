package uk.ac.cam.grpproj.juliet.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import uk.ac.cam.grpproj.juliet.DataPoints;
import uk.ac.cam.grpproj.juliet.financial.Index;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;
import uk.ac.cam.grpproj.juliet.twitter.sentiment.SentimentAggregate;

public class SentimentAggregateDB extends DatabaseHelper {
	public SentimentAggregateDB() throws SQLException {
		super();
	}

	/**
	 * Orders tweets according to created at timestamp.
	 **/
	protected class TweetComparator implements Comparator<Tweet> {
		@Override
		public int compare(Tweet t1, Tweet t2) {
			Date dT1 = t1.getCreatedAt();
			Date dT2 = t2.getCreatedAt();

			return (dT1.compareTo(dT2));
		}
	}

	/**
	 * Very expensive operation used to completely recompute the sentiment
	 * aggregate table. Fetches all tweets from the database, groups them into
	 * 15 minute intervals and computes the sentiment.
	 *
	 * @return void
	 **/
	public void recomputeSentimentAggregate() throws SQLException {
		PreparedStatement stmt = connection.prepareStatement("DELETE FROM "
				+ DatabaseConfig.getSentimentAggregateTableName());
		stmt.executeUpdate();

		CompanyDB cDB = new CompanyDB();
		List<Index> indices = cDB.getIndices();
		cDB.close();

		for (Index index: indices) {
			sentimentAggregate(new Date(0), new Date(), index);
		}
		System.out.println("DONE sentiment aggregation for all indices");
	}

	/**
	 * Performs sentiment aggregation for a certain index over a particular
	 * range.
	 *
	 * @param start
	 *            Start date for the date range
	 * @param end
	 *            End date for the date range
	 * @param index
	 *            Index for which to aggregate sentiment
	 * @return void
	 * @see Index
	 **/
	public void sentimentAggregate(Date start, Date end, Index index)
			throws SQLException {
		System.out.printf(
				"Starting sentiment aggregation for Index %s between %s and %s\n",
				index.getName(),
				convertDateToMySQLDateTime(start),
				convertDateToMySQLDateTime(end));
		TwitterDB tDB = new TwitterDB();
		List<Tweet> tweets = tDB.getTweetsForDateRange(start, end, index);
		tDB.close();

		Tweet lastTweet = null;
		long lastInterval = -1;
		int cntPos = 0, cntNeu = 0, cntNeg = 0, nrTweets = 0;
		for (Tweet tweet : tweets) {
			if (lastTweet != null
					&& (new TweetComparator()).compare(lastTweet, tweet) > 0) {
				throw new SQLException(
						"Tweets are not sorted by creation timestamp");
			} else {
				lastTweet = tweet;
			}

			long newInterval = DataPoints.getIntervalStart(
					tweet.getCreatedAt()).getTime();
			if (lastInterval != -1 && newInterval != lastInterval) {
				SentimentAggregate sa = new SentimentAggregate(
						index.getTicker(), new Date(lastInterval), cntPos,
						cntNeu, cntNeg);
				insertSentimentAggregate(sa);
				cntPos = cntNeu = cntNeg = 0;
			}

			if (tweet.getSentiment() == 1.0) {
				cntPos += 1;
			} else if (tweet.getSentiment() == 0.0) {
				cntNeu += 1;
			} else if (tweet.getSentiment() == -1.0) {
				cntNeg += 1;
			}
			lastInterval = newInterval;
			nrTweets += 1;
		}
		SentimentAggregate sa = new SentimentAggregate(index.getTicker(),
				new Date(lastInterval), cntPos, cntNeu, cntNeg);
		insertSentimentAggregate(sa);

		System.out.println("DONE, processed " + nrTweets + " tweets");
	}

	/**
	 * Insert an entry into the sentiment aggregate table.
	 *
	 * @param SentimentAggregate
	 *            sa
	 * @return void
	 **/
	public void insertSentimentAggregate(SentimentAggregate sa)
			throws SQLException {
		if (sa.getTimestamp().compareTo(DataPoints.getLastAvailableInterval()) == 1) {
			System.out.printf(
					"Sentiment aggregate for %s prematurely computed\n",
					convertDateToMySQLDateTime(sa.getTimestamp()));
			return;
		}
		System.out.printf("New sentiment aggregate %s %s - %d %d %d\n",
				sa.getTicker(), convertDateToMySQLDateTime(sa.getTimestamp()),
				sa.getSentimentPositive(), sa.getSentimentNeutral(),
				sa.getSentimentNegative());
		PreparedStatement stmt = connection
				.prepareStatement("INSERT INTO "
						+ DatabaseConfig.getSentimentAggregateTableName()
						+ " (company_ticker, timestamp, sentiment_positive, sentiment_neutral, sentiment_negative)"
						+ " VALUES (?, ?, ?, ?, ?)");
		stmt.setString(1, sa.getTicker());
		stmt.setString(2, convertDateToMySQLDateTime(sa.getTimestamp()));
		stmt.setInt(3, sa.getSentimentPositive());
		stmt.setInt(4, sa.getSentimentNeutral());
		stmt.setInt(5, sa.getSentimentNegative());

		stmt.executeUpdate();
	}

	/**
	 * Returns the average sentiment for a given index and date range
	 *
	 * @param start
	 *            Start date for the date range
	 * @param end
	 *            End date for the date range
	 * @param index
	 *            Index for which to get sentiment
	 * @return double Average sentiment
	 * @see Index
	 **/
	public Double getSentimentForDateRange(Date start, Date end, Index index)
			throws SQLException, DataPoints.IllegalIntervalException {
		if (!DataPoints.isIntervalStart(start)
				|| !DataPoints.isIntervalEnd(end)
				|| DataPoints.getIntervalLength(start, end)
						% DataPoints.INTERVAL_LENGTH != 0) {
			throw new DataPoints.IllegalIntervalException();
		}

		PreparedStatement stmt = connection
				.prepareStatement("SELECT company_ticker, timestamp, sentiment_positive, sentiment_neutral, sentiment_negative"
						+ " FROM "
						+ DatabaseConfig.getSentimentAggregateTableName()
						+ " WHERE company_ticker = ? AND timestamp BETWEEN ? AND ?");
		stmt.setString(1, index.getTicker());
		stmt.setString(2, convertDateToMySQLDateTime(start));
		stmt.setString(3, convertDateToMySQLDateTime(end));

		ResultSet rs = stmt.executeQuery();
		SentimentAggregate sa = null, cur;
		while (rs.next()) {
			cur = getSentimentAggregateFromResultSet(rs);
			if (sa == null) {
				sa = cur;
			} else {
				sa = sa.add(cur);
			}
		}
		if (sa == null) {
			return 0.0;
		}
		return sa.getSentiment();
	}

	/**
	 * Returns the date of the last stored sentiment aggregate. Useful to
	 * be able to tell where the server left off.
	 *
	 * @param index
	 * @return Date
	 **/
	public Date getLatestAggregateTime(Index index) throws SQLException, ParseException {
		PreparedStatement stmt = connection.prepareStatement(
				"SELECT timestamp FROM " + DatabaseConfig.getSentimentAggregateTableName() +
				" WHERE company_ticker = ?" +
				" ORDER BY timestamp DESC LIMIT 1");
		stmt.setString(1, index.getTicker());
		ResultSet rs = stmt.executeQuery();
		if (!rs.next()) {
		    return null;
		}

		Date date = convertMySQLDateTimeToDate(rs.getString("timestamp"));
		return date;
	}

	private SentimentAggregate getSentimentAggregateFromResultSet(ResultSet rs)
			throws SQLException {
		SentimentAggregate sa = null;
		try {
			sa = new SentimentAggregate(rs.getString("company_ticker"),
					this.convertMySQLDateTimeToDate(rs.getString("timestamp")),
					rs.getInt("sentiment_positive"),
					rs.getInt("sentiment_neutral"),
					rs.getInt("sentiment_negative"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return sa;
	}
}
