package uk.ac.cam.grpproj.juliet.twitter;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import uk.ac.cam.grpproj.juliet.db.CompanyDB;
import uk.ac.cam.grpproj.juliet.db.TwitterDB;
import uk.ac.cam.grpproj.juliet.financial.Company;

public class Tweet implements Serializable {
	private static final long serialVersionUID = 6165971453384013449L;

	private final long tweet_id;
	private Date created_at;
	private long twitter_user_id;
	private TwitterUser twitter_user;
	private long retweets = 0L;
	private String company_ticker;
	private Double sentiment;
	private String text;
	private Company company;
	private String screen_name = "unknown";

	// For TwitterStreamingAPI
	public Tweet(long tweet_id, Date created_at, long twitter_user_id,
			long retweets, String ticker, String text) {
		this.tweet_id = tweet_id;
		this.created_at = created_at;
		this.twitter_user_id = twitter_user_id;
		this.retweets = retweets;
		this.company_ticker = ticker;
		this.text = text;
	}

	// Generic Constructor
	public Tweet(long tweet_id) {
		this.tweet_id = tweet_id;
	}

	public Tweet(long tweet_id, Date created_at, long twitter_user_id,
			long retweets, String company_ticker, String text, Double sentiment) {
		this.tweet_id = tweet_id;
		this.created_at = created_at;
		this.twitter_user_id = twitter_user_id;
		this.retweets = retweets;
		this.company_ticker = company_ticker;
		this.text = text;
		this.sentiment = sentiment;
	}

	public Tweet(long tweet_id, Date date, String screen_name, String text,
			double sentiment) {
		this.tweet_id = tweet_id;
		this.created_at = date;
		this.text = text;
		this.sentiment = sentiment;
		this.screen_name = screen_name;
	}

	public long getID() {
		return tweet_id;
	}

	public Date getCreatedAt() {
		return created_at;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public long getRetweets() {
		return retweets;
	}

	public String getCompanyTicker() {
		return company_ticker;
	}

	public Company getCompany() throws SQLException {
		if (company == null) {
			CompanyDB comp = new CompanyDB();
			company = comp.getCompany(company_ticker);
			comp.close();
		}
		return company;
	}

	public long getTwitterUserID() {
		return twitter_user_id;
	}

	public TwitterUser getTwitterUser() throws SQLException {
		if (twitter_user == null) {
			TwitterDB t = new TwitterDB();
			twitter_user = t.getTwitterUser(twitter_user_id);
			t.close();
		}
		return twitter_user;
	}

	public String getTwitterUsername() {
		return screen_name;
	}

	public void setTwitterUsername(String name) {
		this.screen_name = name;
	}

	public Double getSentiment() {
		return sentiment;
	}

	public void setSentiment(Double sentiment) {
		this.sentiment = sentiment;
	}

	public void setTwitterUser(TwitterUser twitterUser) {
		this.twitter_user = twitterUser;
	}

	public void setCreatedAt(Date created) {
		this.created_at = created;
	}
}
