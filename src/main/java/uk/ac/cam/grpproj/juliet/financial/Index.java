package uk.ac.cam.grpproj.juliet.financial;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.List;

import uk.ac.cam.grpproj.juliet.db.CompanyDB;

public class Index implements Serializable {
	private static final long serialVersionUID = 5052312245436927444L;

	private String ticker;
	private String name;

	public Index(String ticker) throws SQLException {
		this.ticker = ticker;
		CompanyDB cdb = new CompanyDB();
		this.name = cdb.getNameOfIndex(ticker);
		cdb.close();
	}

	public Index(String ticker, String name) {
		this.ticker = ticker;
		this.name = name;
	}

	public String getTicker() {
		return this.ticker;
	}

	public String getName() {
		return this.name;
	}

	public List<Company> getCompanies() throws SQLException {
		CompanyDB cdb = new CompanyDB();
		List<Company> list = cdb.getCompaniesForIndex(this.ticker);
		cdb.close();
		return list;
	}

	@Override
	public String toString() {
		return this.getName();
	}
}
