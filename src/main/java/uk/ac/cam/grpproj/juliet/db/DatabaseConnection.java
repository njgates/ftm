package uk.ac.cam.grpproj.juliet.db;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.mchange.v2.c3p0.DataSources;

public class DatabaseConnection {
	private Session session;
	private static DatabaseConnection instance = null;
	private ComboPooledDataSource cpds;

	private DatabaseConnection() throws SQLException {
		try {
			JSch jsch = new JSch();

			// Get session
			session = jsch.getSession(DatabaseConfig.getSSHUsername(),
					DatabaseConfig.getSSHHost());

			// Set SSH password
			session.setPassword(DatabaseConfig.getSSHPassword());

			// Set host key
			session.setConfig("StrictHostKeyChecking", "no");

			// Connect to session
			session.connect();

			// Setup port forwarding for database
			session.setPortForwardingL(3306, "localhost", 3306);

			// Setup connection pool
			cpds = new ComboPooledDataSource();
			cpds.setDriverClass("com.mysql.jdbc.Driver");
			cpds.setJdbcUrl("jdbc:mysql://" + DatabaseConfig.getDatabaseHost()
					+ "/" + DatabaseConfig.getDatabaseName());
			cpds.setUser(DatabaseConfig.getDatabaseUsername());
			cpds.setPassword(DatabaseConfig.getDatabasePassword());
			cpds.setMinPoolSize(2);
			cpds.setMaxPoolSize(10);
			cpds.setAcquireIncrement(2);

		} catch (JSchException e) {
			System.err.println(e.getMessage());
		} catch (PropertyVetoException e) {
			System.err.println(e.getMessage());
		}
	}

	public static DatabaseConnection getInstance() {
		if (instance == null) {
			try {
				instance = new DatabaseConnection();
			} catch (SQLException e) {
				instance = null;
			}
		}
		return instance;
	}

	public void close() throws SQLException {
		DataSources.destroy(cpds);
		session.disconnect();
	}

	public Connection getConnection() throws SQLException {
		return cpds.getConnection();
	}

}
