package uk.ac.cam.grpproj.juliet.ui.views.playback;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.miginfocom.swing.MigLayout;
import uk.ac.cam.grpproj.juliet.ui.PlaybackPanel;

public class PlaybackControls extends JPanel implements PlaybackPanel {
	private static final long serialVersionUID = -9077758261784789290L;
	private JLabel dateLabel = new JLabel(new Date().toString());

	public PlaybackControls(final PlaybackDelegate delegate,
			int initialSliderSpeed) {
		this.setLayout(new MigLayout("nogrid, fill, insets 0", "[]", "[]"));

		final JButton stopButton = new JButton("Stop");
		final JButton pauseButton = new JButton("Pause");
		final JButton playButton = new JButton("Play");
		// Min 1 minute, Max 4 hours? (in seconds, multiply later)
		final JSlider speedSlider = new JSlider(60, 4 * 60 * 60);

		// Stop Button Listener
		stopButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// Stop button pressed
				playButton.setEnabled(true);
				pauseButton.setEnabled(false);
				stopButton.setEnabled(false);
				speedSlider.setEnabled(false);

				delegate.didStop();
			}

		});

		// Pause Button Listener
		pauseButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// Pause button pressed
				playButton.setEnabled(true);
				stopButton.setEnabled(true);
				pauseButton.setEnabled(false);
				speedSlider.setEnabled(false);

				delegate.didPause();
			}

		});

		// Play Button Listener
		playButton.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// Play button pressed
				playButton.setEnabled(false);
				pauseButton.setEnabled(true);
				stopButton.setEnabled(true);
				speedSlider.setEnabled(true);

				delegate.didPlay();
			}

		});

		// Slider Listener
		speedSlider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent e) {
				delegate.didChangeSpeed(speedSlider.getValue());
			}

		});

		// Inital setup
		stopButton.setEnabled(false);
		pauseButton.setEnabled(false);
		playButton.setEnabled(true);
		speedSlider.setEnabled(false);

		speedSlider.setValue(initialSliderSpeed);

		this.add(stopButton, "cell 0 0");
		this.add(pauseButton, "cell 0 0");
		this.add(playButton, "cell 0 0");
		this.add(speedSlider, "cell 0 0");
		this.add(dateLabel, "cell 0 0");
	}

	@Override
	public void updatePanelToDate(Date date) {
		dateLabel.setText(date.toString());
	}

	@Override
	public void playbackStarted() {

	}

	@Override
	public void playbackResumed() {

	}

	@Override
	public void playbackStopped() {
		dateLabel.setText("");
	}
}
