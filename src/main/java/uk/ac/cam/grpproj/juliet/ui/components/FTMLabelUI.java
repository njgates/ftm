package uk.ac.cam.grpproj.juliet.ui.components;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Point;
import java.awt.RenderingHints;

import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.LabelUI;

import uk.ac.cam.grpproj.juliet.ui.Theme;

public class FTMLabelUI extends LabelUI {
	public static ComponentUI createUI(JComponent c) {
		return new FTMButtonUI();
	}

	@Override
	public void paint(Graphics g, JComponent c) {
		FTMComboBoxLabel b = (FTMComboBoxLabel) c;

		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		// Create Shadow
		g2.setPaint((Paint) Theme.comboShadow);
		g2.drawLine(Theme.borderThickness, c.getHeight() - 1, c.getWidth()
				- Theme.borderThickness, c.getHeight() - 1);

		if (b.isSelected())
			paintButtonSelected(g2, b);
		else
			paintButton(g2, b);

		// Paint text
		this.paintText(g2, b);

	}

	private void paintButton(Graphics2D g2, FTMComboBoxLabel b) {
		g2.setPaint((Paint) Theme.comboNoSel);
		g2.fillRoundRect(Theme.borderThickness, Theme.borderThickness,
				b.getWidth() - (2 * Theme.borderThickness), b.getHeight()
						- (2 * Theme.borderThickness), Theme.borderRadius
						- Theme.borderThickness, Theme.borderRadius
						- Theme.borderThickness);

		// Create highlight
		g2.setPaint((Paint) Theme.highlight);
		g2.drawLine(1 + Theme.borderThickness, Theme.borderThickness,
				b.getWidth() - (2 * Theme.borderThickness),
				Theme.borderThickness);
	}

	private void paintButtonSelected(Graphics2D g2, FTMComboBoxLabel b) {
		g2.setPaint(new GradientPaint(new Point(0, 0), Theme.comboSelGradTop,
				new Point(0, b.getHeight()), Theme.comboSelGradBot));
		g2.fillRoundRect(Theme.borderThickness, Theme.borderThickness,
				b.getWidth() - (2 * Theme.borderThickness), b.getHeight()
						- (2 * Theme.borderThickness), Theme.borderRadius
						- Theme.borderThickness, Theme.borderRadius
						- Theme.borderThickness);
	}

	private void paintText(Graphics g, FTMComboBoxLabel b) {

		Graphics2D g2 = (Graphics2D) g.create();
		g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		// Add text
		// Find the size of string s in font f in the current Graphics context
		// g.
		FontMetrics fm = g.getFontMetrics(g2.getFont());
		java.awt.geom.Rectangle2D rect = fm.getStringBounds(b.getText(), g);

		int textHeight = (int) (rect.getHeight());
		int panelHeight = b.getHeight();

		// Center text vertically
		int y = (panelHeight - textHeight) / 2 + fm.getAscent();
		int x = b.getX() + 3 * Theme.borderThickness;

		g2.setPaint((Paint) Color.black);
		g2.drawString(b.getText(), x + 1, y + 1);

		g2.setPaint(Theme.textColor);
		g2.drawString(b.getText(), x, y);
	}
}
