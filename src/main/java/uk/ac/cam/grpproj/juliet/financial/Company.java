package uk.ac.cam.grpproj.juliet.financial;

import java.io.Serializable;
import java.sql.SQLException;

import uk.ac.cam.grpproj.juliet.db.CompanyDB;

public class Company implements Serializable {
	private static final long serialVersionUID = -4341284153119955908L;

	private final String name;
	private final String ticker;
	private final String index_ticker;
	private Index index;

	public Company(String name, String ticker, String index_ticker) {
		this.name = name;
		this.ticker = ticker;
		this.index_ticker = index_ticker;
	}

	public Company(String ticker) throws SQLException {
		this.ticker = ticker;
		CompanyDB cdb = new CompanyDB();
		Company c = cdb.getCompany(ticker);
		this.name = c.getName();
		this.index_ticker = c.getIndex_ticker();
		cdb.close();
	}

	public String getName() {
		return name;
	}

	public String getTicker() {
		return ticker;
	}

	public String getIndex_ticker() {
		return index_ticker;
	}

	public Index getIndex() throws SQLException {
		if (index == null) {
			CompanyDB c = new CompanyDB();
			index = c.getIndex(index_ticker);
			c.close();
		}
		return index;
	}

	public String toString() {
		return this.name;
	}
}
