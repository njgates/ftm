package uk.ac.cam.grpproj.juliet.ui.views.graph;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import uk.ac.cam.grpproj.juliet.Game;
import uk.ac.cam.grpproj.juliet.GameData;
import uk.ac.cam.grpproj.juliet.GameState;
import uk.ac.cam.grpproj.juliet.financial.Index;
import uk.ac.cam.grpproj.juliet.ui.AppFrame;
import uk.ac.cam.grpproj.juliet.ui.Dialog;
import uk.ac.cam.grpproj.juliet.ui.PlaybackPanel;
import uk.ac.cam.grpproj.juliet.ui.views.graph.components.DatePicker;
import uk.ac.cam.grpproj.juliet.ui.views.graph.components.IndexComboBox;

public class Graph extends JPanel implements PlaybackPanel {
	private static final long serialVersionUID = 5975263931360480190L;

	private AppFrame app;
	private Game game;
	private JPanel graph;
	private IndexComboBox selectIndex;
	private DatePicker from;
	private DatePicker to;
	private DataGraph dg;

	public Graph(final AppFrame app, final Game game) {
		this.app = app;
		this.game = game;
		this.setLayout(new MigLayout("insets 0", "[grow]", "[][grow]"));

		graph = new JPanel();
		graph.setLayout(new MigLayout("fill, insets 0"));

		// Add switches etc
		selectIndex = new IndexComboBox();
		from = new DatePicker(true, this);
		to = new DatePicker(false, this);

		// Set default index
		game.getSettings().setIndex((Index) selectIndex.getSelectedItem());

		// Add listeners
		selectIndex.addActionListener(new IndexSelected());

		updateGraph();
		updateSwitches();

		add(selectIndex, "cell 0 0");
		add(from, "cell 0 0");
		add(to, "cell 0 0");
		add(graph, "cell 0 1,grow");
	}

	private class IndexSelected implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			// change game data to reflect chosen index.
			IndexComboBox cb = (IndexComboBox) e.getSource();
			game.getSettings().setIndex((Index) cb.getSelectedItem());
			game.getGameData().resetMarketPredicted();

			changeStateIfReady();
		}
	}

	public void dateChanged(DatePicker dp) {
		if (dp.isFrom())
			game.getSettings().setDateFrom(dp.getDate());
		else
			game.getSettings().setDateTo(dp.getDate());

		changeStateIfReady();
	}

	private void changeStateIfReady() {
		switch (app.state) {
		case START:
		case INPUT:
			// Check for index, dateFrom and dateTo to be set, if so, then
			// change state
			if (selectIndex.getSelectedItem() != null && to.getDate() != null
					&& from.getDate() != null && dateCheck()) {
				app.state = GameState.INPUT;
				app.stateChanged();
			}
			break;
		default:
			break;
		}
	}

	private boolean dateCheck() {
		String msg = "";
		boolean ret = true;

		// Check dates are in past
		if (to.getDate().after(new Date()) || from.getDate().after(new Date())) {
			ret = false;
			msg = "Please ensure both dates are in the past.";
		}

		// Check to is after from
		if (to.getDate().before(from.getDate())) {
			ret = false;
			msg = "Please ensure that the \"To Date\" is after the \"From Date\".";
		}

		// Check dates aren't the same
		if (to.getDate().equals(from.getDate())) {
			ret = false;
			msg = "Please ensure the dates are not the same.";
		}

		// Check start date isn't before 21/02/2013
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Date d1 = sdf.parse("21/02/2013");
			if (from.getDate().before(d1)) {
				ret = false;
				msg = "Sorry, we don't have any sentiment data from before 21/02/2013";
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}

		// Show dialog if needed
		if (!ret)
			Dialog.showError(app, msg, "Date Error");

		return ret;
	}

	public void update() {
		updateSwitches();
		updateGraph();
	}

	private void updateSwitches() {
		switch (app.state) {
		case START:
			// Load all inputs
			selectIndex.setVisible(true);
			break;
		case INPUT:
			selectIndex.setVisible(true);
			break;
		case DATA:
			selectIndex.setVisible(true);
			break;
		}
	}

	private void updateGraph() {
		graph.removeAll();
		this.validate();
		switch (app.state) {
		case START:
			// Blank Graph Since user is entering name
			graph.add(new InputGraph(new GameData()), "grow");
			break;
		case INPUT:
			// Load and show sentiment graph for selected Index
			graph.add(new InputGraph(game.getGameData()), "grow");
			break;
		case DATA:
			// Load and show data graph
			dg = new DataGraph(game);
			graph.add(dg, "grow");
			this.playbackStopped();
			break;
		default:
			break;
		}
		this.validate();
	}

	@Override
	public void updatePanelToDate(Date date) {
		dg.updatePanelToDate(date);
	}

	@Override
	public void playbackStarted() {
		dg.playbackStarted();
	}

	@Override
	public void playbackResumed() {
		dg.playbackResumed();
	}

	@Override
	public void playbackStopped() {
		dg.playbackStopped();
	}
}
