package uk.ac.cam.grpproj.juliet.ui.views.graph;

import java.awt.event.MouseEvent;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;

import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYSeries;

import uk.ac.cam.grpproj.juliet.GameData;

public class InputGraph extends JPanel implements ChartMouseListener {
	private static final long serialVersionUID = -1527322230562844958L;

	private ChartPanel panel;
	private XYSeries marketPredicted;
	private List<Double> inputEnteredOrder = new ArrayList<Double>();

	private double mousePointX = 0;
	private double mousePointY = 0;

	public InputGraph(GameData data) {
		// get XYSeries (store data points in XYDataItem object) from the game.
		this.marketPredicted = data.getMarketPredicted();

		// Add first price in for the user
		if (data.getMarketActual().getItemCount() > 0)
			marketPredicted.add(data.getMarketActual().getDataItem(0));

		JFreeChart chart = GraphFactory.createInputGraph("Time", "Price", data);
		this.setLayout(new MigLayout("nogrid, fill, insets 0"));

		// Add chart
		panel = new ChartPanel(chart);
		panel.setDomainZoomable(false);
		panel.setRangeZoomable(false);
		panel.addChartMouseListener(this);
		panel.setPopupMenu(null);

		this.add(panel, "grow");
	}

	@Override
	public void chartMouseClicked(ChartMouseEvent e) {
		if (e.getTrigger().getButton() != MouseEvent.BUTTON3) {
			updateMousePosition(e);
			System.out.println("Added value ("
					+ new Date((long) mousePointX).toString() + ", "
					+ mousePointY + ").");
			marketPredicted.add(mousePointX, mousePointY);
			// Adding the input to another list to track the input order which
			// will be used for deletion
			// since the series is always re-sorted when new datapoint is added
			inputEnteredOrder.add(mousePointX);
		} else {
			if (marketPredicted.getItemCount() > 0) {
				System.out.println("Removed last value.");
				// Recall last added element from the list and then removes it
				// using .remove
				int lastElementIndex = (inputEnteredOrder.size() - 1);
				marketPredicted.remove(inputEnteredOrder.get(lastElementIndex));
				inputEnteredOrder.remove(lastElementIndex);
			}

		}
	}

	@Override
	public void chartMouseMoved(ChartMouseEvent e) {
		updateMousePosition(e);
		XYPlot plot = (XYPlot) e.getChart().getPlot();
		plot.setDomainCrosshairValue(mousePointX);
		plot.setRangeCrosshairValue(mousePointY);
	}

	private void updateMousePosition(ChartMouseEvent e) {
		Point2D p = panel.translateScreenToJava2D(e.getTrigger().getPoint());
		Rectangle2D plotArea = panel.getScreenDataArea();
		XYPlot plot = (XYPlot) e.getChart().getPlot();
		mousePointX = plot.getDomainAxis().java2DToValue(p.getX(), plotArea,
				plot.getDomainAxisEdge());
		mousePointY = plot.getRangeAxis().java2DToValue(p.getY(), plotArea,
				plot.getRangeAxisEdge());
	}
}
