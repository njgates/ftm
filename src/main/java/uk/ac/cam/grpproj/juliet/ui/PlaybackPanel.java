package uk.ac.cam.grpproj.juliet.ui;

import java.util.Date;

public interface PlaybackPanel {

	public void updatePanelToDate(Date date);

	public void playbackStarted();

	public void playbackResumed();

	public void playbackStopped();

}
