package uk.ac.cam.grpproj.juliet.db;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import uk.ac.cam.grpproj.juliet.financial.Company;
import uk.ac.cam.grpproj.juliet.financial.Index;

public class CompanyDB extends DatabaseHelper {
	public CompanyDB() throws SQLException {
		super();
	}

	public List<Company> getCompanies() throws SQLException {
		String sql = "SELECT * FROM " + DatabaseConfig.getCompaniesTableName()
				+ " ORDER BY name ASC";
		ResultSet results = connection.createStatement().executeQuery(sql);

		List<Company> companies = new LinkedList<Company>();
		while (results.next()) {
			companies.add(new Company(results.getString("ticker")));
		}
		return companies;
	}

	public List<Index> getIndices() throws SQLException {
		String sql = "SELECT * FROM " + DatabaseConfig.getIndicesTableName()
				+ " ORDER BY name ASC";
		ResultSet results = connection.createStatement().executeQuery(sql);

		List<Index> indices = new LinkedList<Index>();
		while (results.next()) {
			indices.add(new Index(results.getString("ticker"), results
					.getString("name")));
		}
		return indices;
	}

	public String getNameOfIndex(String ticker) throws SQLException {
		String sql = "SELECT name FROM " + DatabaseConfig.getIndicesTableName()
				+ " WHERE ticker=\"" + ticker + "\"";
		ResultSet results = connection.createStatement().executeQuery(sql);

		results.first();
		String res = results.getString("name");

		if (res == null) {
			System.err.println("Could not get index name");
		}

		return res;
	}

	public Company getCompany(String company_ticker) throws SQLException {
		String sql = "SELECT * FROM " + DatabaseConfig.getCompaniesTableName()
				+ " WHERE ticker=\"" + company_ticker + "\"";

		ResultSet results = connection.createStatement().executeQuery(sql);

		results.first();
		Company c = new Company(results.getString("name"),
				results.getString("ticker"), results.getString("index_ticker"));
		return c;
	}

	public List<Company> getCompaniesForIndex(String index_ticker)
			throws SQLException {
		String sql = "SELECT * FROM " + DatabaseConfig.getCompaniesTableName()
				+ " WHERE index_ticker=\"" + index_ticker + "\"";
		ResultSet results = connection.createStatement().executeQuery(sql);

		List<Company> companies = new LinkedList<Company>();
		while (results.next()) {
			companies.add(new Company(results.getString("name"), results
					.getString("ticker"), index_ticker));
		}
		return companies;
	}

	public Index getIndex(String index_ticker) throws SQLException {
		String sql = "SELECT * FROM " + DatabaseConfig.getIndicesTableName()
				+ " WHERE ticker=\"" + index_ticker + "\"";

		ResultSet results = connection.createStatement().executeQuery(sql);

		results.first();
		Index i = new Index(results.getString("ticker"),
				results.getString("name"));
		return i;
	}
}
