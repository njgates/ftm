package uk.ac.cam.grpproj.juliet.ui.views.graph;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jfree.data.xy.XYDataItem;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import uk.ac.cam.grpproj.juliet.ui.PlaybackPanel;

public class DynamicXYSeriesCollection extends XYSeriesCollection implements
PlaybackPanel {
	private static final long serialVersionUID = 6725951536337974047L;

	List<XYSeries> pseries = new ArrayList<XYSeries>();
	List<XYSeries> oseries = new ArrayList<XYSeries>();
	List<Integer> seriesIndex = new ArrayList<Integer>();

	public void addSeries(XYSeries original, XYSeries playback) {
		// Keep copy of original and playback series
		oseries.add(original);
		pseries.add(playback);
		seriesIndex.add(0);
		// Add to actual dataset
		super.addSeries(playback);
	}

	@Override
	public void updatePanelToDate(Date d) {
		for (int i = 0; i < pseries.size(); i++) {
			// System.out.println("Update for Data series:"+i);
			// iterate through each series in the original series and create a
			// play back for each
			XYSeries p = pseries.get(i);
			XYSeries o = oseries.get(i);
			Integer index = seriesIndex.get(i);

			// check for zero dataitem in the playback series.
			// so if the playback starts with no value at all then add in the
			// first data point
			// from the original series
			if (p.getItemCount() == 0) {
				p.add(o.getX(0), o.getY(0));
				index++;
			}

			// Get latest date in playback series
			Date latest = new Date(p.getX(p.getItemCount() - 1).longValue());

			// Add in most recent data points still before update to date
			while (latest.before(d) && index < o.getItemCount()) {

				XYDataItem item = (XYDataItem) o.getItems().get(index);
				latest = new Date(item.getX().longValue());
				// if the new point to be added is before the date d
				if (latest.before(d)) {
					p.add(item);
					index++;
				}
				/*
				 * if not then take the previous point and this new point and
				 * interpolate to find an intermediate value which lies on the
				 * Date d. After adding, exit the current for loop and continues
				 * to the next one.
				 */
			}
			/*
			 * //////////////////////////////////////////////////////////////////
			 * /////////////////////////////////////////
			 * System.out.println(": Value of date at the end while loop");
			 * System.out.println(d.getTime()+": Value of date to update to");
			 * System.out.println(o.getItems().get(index)+
			 * ": Value of the last point before cut off");
			 * System.out.println(o.getItems().get(index+1)+
			 * ": Value of the next point in original series");
			 */// /////////////////////////////////////////////////////////////////////////////////////////////////////////
			if (index < o.getItemCount()) {
				// System.out.println("Entering interpolation mode");
				// Also need to check whether the point is the last point in the
				// series
				// if it is then stop and go no further since this will only
				// lead to
				// IndexOutOfBoundException
				if (p.getItemCount() == 0) {
					p.add(o.getX(0), o.getY(0));
					index++;
				}
				if(index == 0){
					index++;
				}
				else{
					XYDataItem lastDataPointBeforeCutOff = (XYDataItem) o
							.getItems().get(index - 1);
					XYDataItem firstDataPointAfterCutOff = (XYDataItem) o
							.getItems().get(index);
					XYDataItem interpolatedPoint;

					// Interpolate by using normal straight line equation: y= mx+c
					double x1 = lastDataPointBeforeCutOff.getXValue();
					double y1 = lastDataPointBeforeCutOff.getYValue();
					double x2 = firstDataPointAfterCutOff.getXValue();
					double y2 = firstDataPointAfterCutOff.getYValue();
					double interpolatedYValue = y1 + (d.getTime() - x1)
							* ((y2 - y1) / (x2 - x1));
					//System.out.println("(" + d.getTime() + "," + interpolatedYValue
					//		+ ") : New Interpolated point added");
					interpolatedPoint = new XYDataItem(d.getTime(),
							interpolatedYValue);
					p.add(interpolatedPoint);
				}
			}

		}
	}

	@Override
	public void playbackStarted() {
		for (@SuppressWarnings("unused")
		Integer i : seriesIndex) {
			i = 0;
		}
		for (XYSeries s : pseries) {
			for (int i = 0; i < s.getItemCount(); i++)
				s.remove(0);
		}
		super.removeAllSeries();
		for (XYSeries s : pseries)
			super.addSeries(s);
	}

	@Override
	public void playbackResumed() {
	}

	@Override
	public void playbackStopped() {
		// FIXME fix this method

		super.removeAllSeries();
		// Add original series in
		for (XYSeries s : oseries)
			super.addSeries(s);

		// Remove data items
		for (XYSeries p : pseries) {
			p.clear();
		}

		// Reset indices
		for (@SuppressWarnings("unused")
		Integer i : seriesIndex) {
			i = 0;
		}
	}
}
