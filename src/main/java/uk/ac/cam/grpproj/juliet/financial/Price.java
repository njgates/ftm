package uk.ac.cam.grpproj.juliet.financial;

import java.util.Date;

public class Price {

	// decided to not compose this class with a Company field because would
	// require unnecessary database accesses
	private final String ticker;
	private final double price;
	private final Date date;

	public Price(String ticker, double price, Date date) {
		this.ticker = ticker;
		this.price = price;
		this.date = date;
	}

	public String getTicker() {
		return ticker;
	}

	public double getPrice() {
		return price;
	}

	public Date getDate() {
		return date;
	}

}
