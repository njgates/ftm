package uk.ac.cam.grpproj.juliet.twitter.feed;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import uk.ac.cam.grpproj.juliet.financial.Company;
import uk.ac.cam.grpproj.juliet.twitter.Tweet;
import uk.ac.cam.grpproj.juliet.twitter.TwitterUser;

public class TwitterFilter implements StatusListener {

	private Map<String, Company> keywords;
	private TweetProcessor processor;
	private Integer count = null;

	public TwitterFilter(Map<String, Company> keywords, TweetProcessor processor) {
		this.keywords = keywords;
		this.processor = processor;
	}

	@Override
	public void onException(Exception e) {
		e.printStackTrace();
	}

	@Override
	public void onDeletionNotice(StatusDeletionNotice arg0) {
		// Don't care
	}

	@Override
	public void onScrubGeo(long arg0, long arg1) {
		// Huh?
	}

	@Override
	public void onStallWarning(StallWarning warning) {
		System.out.println("TwitterAPI: " + warning.getMessage());
	}

	@Override
	public void onStatus(Status status) {
		List<String> tags = getKeywordsMatchingTweet(status);

		if (tags.size() != 1)
			return;

		if (!status.getUser().getLang().equals("en"))
			return;

		// Check for english tweet
		boolean english = false;
		int i = 0;
		String[] words = status.getText().split(" ");
		while (!english && i < words.length) {
			if (WordList.getInstance().isWordEnglish(words[i]))
				english = true;
			i++;
		}
		if (!english)
			return;

		// implement more filters here

		Tweet tweet = new Tweet(status.getId(), status.getCreatedAt(), status
				.getUser().getId(), status.getRetweetCount(), this.keywords
				.get(tags.get(0)).getTicker(), status.getText());

		TwitterUser user = new TwitterUser(status.getUser().getId(), status
				.getUser().getScreenName(), status.getUser()
				.getFollowersCount(), status.getUser().getLocation());

		tweet.setTwitterUser(user);

		processor.onReceiveTweet(tweet);
	}

	@Override
	public void onTrackLimitationNotice(int n) {
		// So we miss a few tweets....who cares!
		// Log it though
		System.out.println("Missed " + n + " tweets due to api limitations.");
	}

	private List<String> getKeywordsMatchingTweet(Status status) {
		// Match tweets against all tracked strings to determine matches.
		String tweetText = status.getText();
		List<String> tags = new LinkedList<String>();
		for (String s : keywords.keySet()) {
			boolean match = false;
			if (trackedStringIsTag(s)) {
				match = tweetText.contains(s);
			} else {
				match = tweetText.toLowerCase().matches(
						".*(^|\\W)\\Q" + s.toLowerCase() + "\\E(\\W|$).*");
			}

			if (match) {
				tags.add(s);
			}
		}
		return tags;
	}

	private boolean trackedStringIsTag(String str) {
		return str.startsWith("$") || str.startsWith("#");
	}
}
