package uk.ac.cam.grpproj.juliet.ui.views.instructions;

import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import uk.ac.cam.grpproj.juliet.ui.Theme;

public class InstructionsView extends JPanel {
	private static final long serialVersionUID = 855810105335865692L;

	public InstructionsView() {
		setLayout(new MigLayout("nogrid, fill"));
		JLabel label = new JLabel("<html><h2>INSTRUCTIONS</h2><ol><li>Select an index: FTSE or NASDAQ</li><li>Select a date range using the date pickers. Ensure the start date is not before 21/02/2013</li><li>The graph to your right will display the Twitter Sentiment Data for that period. Use this data to estimate how that index developed during that time period.<ul><li>Click on the graph to enter a datapoint</li><li>Right click to remove the previous point</li></ul></li><li>Click Submit.</li><li>The graph will now display the actual market data and your predication, along with the most popular tweets for than index.</li><li>Press 'Play' to watch how the market, sentiment and tweets played out in real time.</li></ol></html>");
		
		label.setForeground(Theme.textColor);
		add(label, "growx");
	}

}
