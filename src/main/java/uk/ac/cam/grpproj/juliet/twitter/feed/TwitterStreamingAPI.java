package uk.ac.cam.grpproj.juliet.twitter.feed;

import java.io.Closeable;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import twitter4j.FilterQuery;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.ConfigurationBuilder;
import uk.ac.cam.grpproj.juliet.financial.Company;
import uk.ac.cam.grpproj.juliet.financial.Index;

public class TwitterStreamingAPI extends Thread implements Closeable {

	List<Index> indices;
	List<TwitterStream> streams = new LinkedList<TwitterStream>();
	Integer count;

	public TwitterStreamingAPI(List<Index> indicesToFollow) {
		this.indices = indicesToFollow;
	}

	@Override
	public void run() {
		for (int i = 0; i < indices.size(); i++) {
			Index index = indices.get(i);
			try {
				// Start new stream
				TwitterStream ts = new TwitterStreamFactory(getConfig(i)
						.build()).getInstance();
				Map<String, Company> keywords = TwitterKeywords
						.getKeywordsForIndex(index);

				ts.addListener(new TwitterFilter(keywords, new TweetProcessor()));

				FilterQuery fq = new FilterQuery();
				fq.track(keywords.keySet().toArray(new String[keywords.size()]));
				ts.filter(fq);
				streams.add(ts);
				System.out.println("Start stream for " + index.getName());
			} catch (SQLException e) {
				System.out.println("Can't start Twitter stream for index: "
						+ index.getName());
			}
		}
	}

	@Override
	public void close() throws IOException {
		for (TwitterStream t : streams) {
			t.shutdown();
		}
	}

	private ConfigurationBuilder getConfig(int n) {
		ConfigurationBuilder cb = new ConfigurationBuilder();
		switch (n) {
		case 0:
			cb.setOAuthConsumerKey("avubTxffrMir1OcoVHImg")
					.setOAuthConsumerSecret(
							"tFxbWlRN2TqDRGxLSE7KRgwCCG7JJrAzI4S1fLqwSIE")
					.setOAuthAccessToken(
							"19667153-g3BEXuGd1b0pMhP2pZ8EzmqRJLGnVnYgrlJT4gz2A")
					.setOAuthAccessTokenSecret(
							"req8Q9BIhHqI6dL9j0tjkulBmMuytf0bq20FXZQwYA");
			break;
		case 1:
			cb.setOAuthConsumerKey("AIg89YDn6NbR8Rzcfk6Mjw")
					.setOAuthConsumerSecret(
							"w5hFSsznlve0yPOCkhQWnGa7nvMjdDi7EUMLAVAISI")
					.setOAuthAccessToken(
							"19667153-ZReZdTgd41ZhUSAOEtpGRUbyP8DX4LEtfv1nMvJZs")
					.setOAuthAccessTokenSecret(
							"H1Qi3LnCIgBoowimR4QkgR4Zf3S3bRAq52EkH624");
			break;
		default:
			System.err.println("Not enough configurations built in.");
		}
		return cb;
	}

}
