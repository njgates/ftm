package uk.ac.cam.grpproj.juliet.ui.views.graph.components;

import java.sql.SQLException;
import java.util.List;

import uk.ac.cam.grpproj.juliet.db.CompanyDB;
import uk.ac.cam.grpproj.juliet.financial.Index;

public class IndexComboBox extends FTMComboBox<Index> {
	private static final long serialVersionUID = -8875767250344848044L;

	public IndexComboBox() {
		CompanyDB cdb = null;
		try {
			cdb = new CompanyDB();
			List<Index> indices = cdb.getIndices();

			// Add indices to indexBox
			for (Index i : indices)
				addItem(i); // String for same reason as 2

			cdb.close();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
}
