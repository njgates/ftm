package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import uk.ac.cam.grpproj.juliet.financial.Index;
import uk.ac.cam.grpproj.juliet.db.CompanyDB;
import uk.ac.cam.grpproj.juliet.db.SentimentAggregateDB;
import uk.ac.cam.grpproj.juliet.DataPoints;

public class SentimentAggregateThread extends Thread {
	@Override
	public void run() {
		List<Index> indices;

		try {
			CompanyDB cDB = new CompanyDB();
			indices = cDB.getIndices();
			cDB.close();
		} catch (SQLException e) {
			e.printStackTrace();
			return;
		}

		for (; ; ) {
			try {
				SentimentAggregateDB sDB = new SentimentAggregateDB();
				for (Index index: indices) {
					Date startDate = sDB.getLatestAggregateTime(index);
					if (startDate == null) {
						startDate = new Date(0);
					} else {
						startDate = DataPoints.getNextIntervalStart(startDate);
					}
					Date endDate = DataPoints.getLastAvailableInterval();
					endDate = DataPoints.getIntervalEnd(endDate);

					if (startDate.compareTo(endDate) <= 0) {
						sDB.sentimentAggregate(startDate, endDate, index);
					}
				}
				sDB.close();
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				e.printStackTrace();
			}
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
