package uk.ac.cam.grpproj.juliet.ui;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

public class Dialog {

	public static void showError(JFrame sender, String message, String title) {
		JOptionPane.showMessageDialog(sender, getTextAreaForMessage(message),
				title, JOptionPane.ERROR_MESSAGE);
	}

	public static void showWarning(JFrame sender, String message, String title) {
		JOptionPane.showMessageDialog(sender, getTextAreaForMessage(message),
				title, JOptionPane.WARNING_MESSAGE);
	}

	public static String showUserInput(JFrame sender, String message,
			String title) {
		return JOptionPane.showInputDialog(sender,
				getTextAreaForMessage(message), "Welcome",
				JOptionPane.PLAIN_MESSAGE);
	}

	private static JTextArea getTextAreaForMessage(String message) {
		JTextArea ja = new JTextArea();
		ja.setText(message);
		ja.setForeground(Theme.textColor);
		ja.setOpaque(false);

		return ja;
	}
}
