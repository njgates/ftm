package uk.ac.cam.grpproj.juliet;

import java.util.Date;

public class DataPoints {
	// 15 minutes in milliseconds
	public static final long INTERVAL_LENGTH = 15 * 60 * 1000;

	public static class IllegalIntervalException extends Exception {
		private static final long serialVersionUID = 1906421417745725498L;
	};

	/**
	 * Given a timestamp, returns the beginning timestamp of the 15 minute
	 * interval containing it.
	 *
	 * @param date
	 * @return Date
	 **/
	public static Date getIntervalStart(Date date) {
		long time = date.getTime();
		time -= time % INTERVAL_LENGTH;
		return new Date(time);
	}

	/**
	 * Given a timestamp, returns the end timestamp of the 15 minute interval
	 * containing it.
	 *
	 * @param date
	 * @return Date
	 **/
	public static Date getIntervalEnd(Date date) {
		long time = date.getTime();
		time -= time % INTERVAL_LENGTH;
		time += INTERVAL_LENGTH - 1000;
		return new Date(time);
	}

	/**
	 * Given a timestamp, returns the beginning timestamp of the 15 minute
	 * interval immediately after the one containing it.
	 *
	 * @param date
	 * @return Date
	 **/
	public static Date getNextIntervalStart(Date date) {
		long time = date.getTime();
		time -= time % INTERVAL_LENGTH;
		time += INTERVAL_LENGTH;
		return new Date(time);
	}

	/**
	 * Given two timestamps, returns the length of the interval between them.
	 *
	 * @param start
	 * @param end
	 * @return long
	 **/
	public static long getIntervalLength(Date start, Date end) {
		return end.getTime() - start.getTime() + 1000;
	}

	/**
	 * Given a timestamp, returns whether or not it is aligned to an interval
	 * start point.
	 *
	 * @param date
	 * @return boolean
	 **/
	public static boolean isIntervalStart(Date date) {
		return date.equals(getIntervalStart(date));
	}

	/**
	 * Given a timestamp, returns whether or not it is aligned to an interval
	 * end point.
	 *
	 * @param date
	 * @return boolean
	 **/
	public static boolean isIntervalEnd(Date date) {
		return date.equals(getIntervalEnd(date));
	}

	/**
	 * Returns the start of the latest 15-minute interval ending before the
	 * current time. Used to determine when a new aggregate needs to be
	 * computed.
	 *
	 * @return Date
	 **/
	public static Date getLastAvailableInterval() {
		// Get start of current interval
		long time = getIntervalStart(new Date()).getTime();
		// Get start of previous inteval
		time -= INTERVAL_LENGTH;
		return new Date(time);
	}
}
