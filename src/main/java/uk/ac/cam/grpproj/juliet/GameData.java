package uk.ac.cam.grpproj.juliet;

import java.io.Serializable;
import java.util.List;

import org.jfree.data.xy.XYSeries;

import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public class GameData implements Serializable {
	private static final long serialVersionUID = -3563794830869590199L;

	private List<Tweet> tweets;
	private XYSeries sentiment;
	private XYSeries marketActual;
	private XYSeries marketPredicted = null;

	public XYSeries getSentiment() {
		if (sentiment == null)
			sentiment = new XYSeries("Sentiment");
		return sentiment;
	}

	public void setSentiment(XYSeries sentiment) {
		this.sentiment = sentiment;
	}

	public XYSeries getMarketActual() {
		if (marketActual == null)
			marketActual = new XYSeries("Market");
		return marketActual;
	}

	public void setMarketActual(XYSeries marketActual) {
		this.marketActual = marketActual;
	}

	public XYSeries getMarketPredicted() {
		if (marketPredicted == null)
			marketPredicted = new XYSeries("Prediction");
		return marketPredicted;
	}

	public void resetMarketPredicted() {
		if (marketPredicted != null) {
			for (int i = marketPredicted.getItemCount() - 1; i >= 0; i--)
				marketPredicted.remove(i);
		}
	}

	public void setTweets(List<Tweet> tweets) {
		this.tweets = tweets;
	}

	public List<Tweet> getTweets() {
		return tweets;
	}
}
