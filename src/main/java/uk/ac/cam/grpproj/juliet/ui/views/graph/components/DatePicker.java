package uk.ac.cam.grpproj.juliet.ui.views.graph.components;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;

import net.miginfocom.swing.MigLayout;
import net.sourceforge.jdatepicker.DateModel;
import net.sourceforge.jdatepicker.JDateComponentFactory;
import net.sourceforge.jdatepicker.JDatePicker;
import uk.ac.cam.grpproj.juliet.ui.views.graph.Graph;

public class DatePicker extends JPanel {
	private static final long serialVersionUID = -3243804716622866365L;
	private JDatePicker picker;
	private Graph graph = null;
	private boolean from;

	public DatePicker(boolean from, Graph graph) {
		this.graph = graph;
		this.from = from;
		setLayout(new MigLayout("nogrid, insets 0"));

		JLabel label = (from) ? new JLabel("From: ") : new JLabel("To: ");

		DateModel<?> model = JDateComponentFactory.createDateModel(Date.class);

		picker = JDateComponentFactory.createJDatePicker(model);

		picker.setTextEditable(true);
		picker.addActionListener(new DateChanged());

		add(label);
		add((Component) picker);
	}

	private class DateChanged implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			graph.dateChanged(DatePicker.this);
		}
	}

	public Date getDate() {
		return (Date) picker.getModel().getValue();
	}

	public boolean isFrom() {
		return from;
	}

}
