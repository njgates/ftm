package uk.ac.cam.grpproj.juliet.twitter;

public class TwitterUser {
	private final long twitter_id;
	private final String screen_name;
	private final long follower_count;
	private final String location;

	public TwitterUser(long twitter_id, String screen_name,
			long follower_count, String location) {
		this.twitter_id = twitter_id;
		this.screen_name = screen_name;
		this.follower_count = follower_count;
		this.location = location;
	}

	public long getID() {
		return this.twitter_id;
	}

	public String getScreenName() {
		return this.screen_name;
	}

	public long getFollowerCount() {
		return this.follower_count;
	}

	public String getLocation() {
		return this.location;
	}
}
