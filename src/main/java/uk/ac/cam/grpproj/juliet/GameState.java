package uk.ac.cam.grpproj.juliet;

public enum GameState {
	START, INPUT, DATA
}
