package uk.ac.cam.grpproj.juliet.ui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.border.Border;

import uk.ac.cam.grpproj.juliet.ui.components.FTMButtonUI;

public class Theme {

	private static UIDefaults d = UIManager.getLookAndFeelDefaults();

	public static Color bgColor = new Color(57, 64, 70);
	public static Color bgColor2 = new Color(42, 51, 58);
	public static Border frameBorder = BorderFactory.createLineBorder(
			new Color(27, 27, 27), 2);

	public static Color topGradient = new Color(50, 61, 67);
	public static Color bottomGradient = new Color(36, 43, 49);
	public static Color borderGradTop = new Color(29, 38, 45);
	public static Color borderGradBottom = new Color(17, 22, 25);
	public static Color highlight = new Color(69, 81, 93);
	public static Color pressed = new Color(38, 45, 51);

	public static Color comboBG = new Color(42, 51, 58);
	public static Color comboSel = new Color(56, 66, 75);

	public static Color comboSelHighlight = new Color(92, 109, 125);
	public static Color comboSelGradTop = new Color(57, 67, 77);
	public static Color comboSelGradBot = new Color(56, 64, 74);
	public static Color comboNoSel = new Color(43, 53, 60);
	public static Color comboShadow = new Color(27, 36, 41);

	public static Color darkGradTop = new Color(24, 25, 27);
	public static Color darkGradBot = new Color(9, 10, 12);

	public static int borderRadius = 6;
	public static int borderThickness = 2;

	public static Font textFont = new Font(null, Font.PLAIN, 20);
	public static Color textColor = new Color(236, 236, 236);
	public static Color textDisabled = new Color(103, 103, 103);

	public static void setTheme() {
		// Set Backgrounds
		d.put("Panel.background", bgColor);
		d.put("OptionPane.background", bgColor);
		d.put("ScrollBar.background", bgColor2);

		// Set Borders
		d.put("OptionPane.border", frameBorder);

		// Set foreground colors
		changeForeground(textColor);

		// Set UIs
		// UIManager.put("ScrollBarUI", FTMScrollBarUI.class.getName());
		UIManager.put("ButtonUI", FTMButtonUI.class.getName());
	}

	private static void changeForeground(Color sf) {
		UIManager.put("Button.foreground", sf);
		UIManager.put("ToggleButton.foreground", sf);
		UIManager.put("RadioButton.foreground", sf);
		UIManager.put("CheckBox.foreground", sf);
		UIManager.put("ColorChooser.foreground", sf);
		UIManager.put("ToggleButton.foreground", sf);
		// UIManager.put("ComboBox.foreground",sf);
		// UIManager.put("ComboBoxItem.foreground",sf);
		UIManager.put("InternalFrame.titleforeground", sf);
		UIManager.put("Label.foreground", sf);
		// UIManager.put("List.foreground",sf);
		UIManager.put("MenuBar.foreground", sf);
		UIManager.put("Menu.foreground", sf);
		// UIManager.put("MenuItem.foreground",sf);
		UIManager.put("RadioButtonMenuItem.foreground", sf);
		UIManager.put("CheckBoxMenuItem.foreground", sf);
		UIManager.put("PopupMenu.foreground", sf);
		UIManager.put("OptionPane.foreground", sf);
		UIManager.put("Panel.foreground", sf);
		UIManager.put("ProgressBar.foreground", sf);
		UIManager.put("ScrollPane.foreground", sf);
		UIManager.put("Viewport", sf);
		UIManager.put("TabbedPane.foreground", sf);
		UIManager.put("EditorPane.foreground", sf);
		UIManager.put("TitledBorder.foreground", sf);
		UIManager.put("ToolBar.foreground", sf);
		UIManager.put("ToolTip.foreground", sf);
		UIManager.put("Tree.foreground", sf);
	}
}
