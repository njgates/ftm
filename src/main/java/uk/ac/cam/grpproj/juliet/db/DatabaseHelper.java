package uk.ac.cam.grpproj.juliet.db;

import java.io.Closeable;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class DatabaseHelper implements Closeable {

	protected Connection connection;

	public DatabaseHelper() throws SQLException {
		connection = DatabaseConnection.getInstance().getConnection();
	}

	protected String convertDateToMySQLDateTime(Date date) {
		DateFormat dfSQL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dfSQL.format(date);
	}

	protected Date convertMySQLDateTimeToDate(String date)
			throws ParseException {
		DateFormat dfSQL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return dfSQL.parse(date);
	}

	protected String convertToDateTime(String date, String time)
			throws ParseException {
		// MySQL Format: 'YYYY-MM-DD HH:MM:SS'
		// CSV Format: 'YYYY.MM.DD' 'HH:MM'
		DateFormat dfCSV = new SimpleDateFormat("yyyy.MM.dd HH:mm");
		DateFormat dfSQL = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date d = dfCSV.parse(date + " " + time);
		return dfSQL.format(d);
	}

	@Override
	public void close() {
		try {
			connection.close();
		} catch (SQLException e) {
			System.out.println("Failed to close connection.");
		}

	}
}
