package uk.ac.cam.grpproj.juliet.ui.views.graph;

import java.awt.Color;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.StandardChartTheme;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.StandardXYItemRenderer;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeriesCollection;

import uk.ac.cam.grpproj.juliet.GameData;

//Graph to view data and results on
public abstract class GraphFactory {
	public static JFreeChart createInputGraph(String xAxisLabel,
			String yAxisLabel, GameData data) {

		// Make dataset out of XYSeries marketPredicted
		XYSeriesCollection dataset = new XYSeriesCollection(
				data.getMarketPredicted());

		boolean legend = false;
		boolean tooltips = false;
		boolean urls = false;
		JFreeChart chart = ChartFactory.createXYLineChart(null, xAxisLabel,
				yAxisLabel, dataset, PlotOrientation.VERTICAL, legend,
				tooltips, urls);

		XYPlot plot = chart.getXYPlot();

		// Set x-axis properties
		plot.setDomainCrosshairVisible(true);
		plot.setDomainCrosshairLockedOnData(true);
		plot.setDomainAxis(new DateAxis());
		plot.getDomainAxis().setAutoRange(false);
		double minX = data.getMarketActual().getMinX();
		double maxX = data.getMarketActual().getMaxX();
		double twoHours = 1000 * 60 * 60 * 2;
		plot.getDomainAxis().setRange(minX - twoHours, maxX + twoHours);

		// Set y-axis properties
		plot.setRangeCrosshairVisible(true);
		plot.setRangeCrosshairLockedOnData(true);
		plot.getRangeAxis().setAutoRange(true);
		double maxY = data.getMarketActual().getMaxY();
		double minY = data.getMarketActual().getMinY();
		plot.getRangeAxis().setRangeWithMargins(minY, maxY);

		// Set sentiment axis properties
		plot.setRangeAxis(1, new NumberAxis("Sentiment"));
		plot.getRangeAxis(1).setAutoRange(true);
		plot.setDataset(1, new XYSeriesCollection(data.getSentiment()));
		plot.mapDatasetToRangeAxis(1, 1);

		// Set theme for market prediction
		themeInputGraph(chart, plot);

		return chart;
	}

	public static JFreeChart createDataGraph(String xAxisLabel,
			String yAxisLabel, XYDataset dataset, GameData data) {
		boolean legend = true;
		boolean tooltips = true;
		boolean urls = false;
		JFreeChart chart = ChartFactory.createXYLineChart(null, xAxisLabel,
				yAxisLabel, dataset, PlotOrientation.VERTICAL, legend,
				tooltips, urls);

		XYPlot plot = chart.getXYPlot();

		// Set x-axis properties
		plot.setDomainAxis(new DateAxis());
		plot.getDomainAxis().setAutoRange(false);
		double minX = data.getMarketActual().getMinX();
		double maxX = data.getMarketActual().getMaxX();
		double twoHours = 1000 * 60 * 60 * 2;
		plot.getDomainAxis().setRange(minX - twoHours, maxX + twoHours);

		// Set y-axis properties
		plot.getRangeAxis().setAutoRange(true);
		double maxY = data.getMarketActual().getMaxY();
		double minY = data.getMarketActual().getMinY();
		plot.getRangeAxis().setRangeWithMargins(minY, maxY);

		// Set sentiment axis properties
		plot.setRangeAxis(1, new NumberAxis("Sentiment"));
		plot.mapDatasetToRangeAxis(1, 1);
		double maxYSen = data.getSentiment().getMaxY();
		double minYSen = data.getSentiment().getMinY();
		plot.getRangeAxis(1).setRangeWithMargins(minYSen, maxYSen);
		themeDataGraph(chart, plot);

		return chart;
	}

	public static void themeInputGraph(JFreeChart chart, XYPlot plot) {
		plot.setRenderer(1, new StandardXYItemRenderer());
		plot.setRenderer(0, new XYLineAndShapeRenderer());
		StandardChartTheme sct = (StandardChartTheme) StandardChartTheme
				.createDarknessTheme();

		sct.apply(chart);
	}

	public static void themeDataGraph(JFreeChart chart, XYPlot plot) {
		StandardChartTheme sct = (StandardChartTheme) StandardChartTheme
				.createDarknessTheme();
		sct.apply(chart);
		StandardXYItemRenderer renderer1 = new StandardXYItemRenderer();
		StandardXYItemRenderer renderer2 = new StandardXYItemRenderer();
		renderer1.setSeriesPaint(0, Color.BLUE);
		renderer1.setSeriesPaint(1, Color.RED);
		plot.setRenderer(0, renderer1);
		plot.setRenderer(1, renderer2);
		
		/*
		 * CustomGradientXYAreaRenderer renderer = new
		 * CustomGradientXYAreaRenderer(); plot.setRenderer(0, renderer);
		 * renderer.setSeriesFillPaint(0, new GradientPaint(0f, 0f, Color.green,
		 * 0f, 0f, Color.red)); renderer.setOutline(true);
		 * renderer.setSeriesOutlinePaint(0, Color.black);
		 * plot.setForegroundAlpha(0.6f);
		 */
	}
}
