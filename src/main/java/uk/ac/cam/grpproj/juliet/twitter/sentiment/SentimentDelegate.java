package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.util.List;

import uk.ac.cam.grpproj.juliet.twitter.Tweet;

public interface SentimentDelegate {

	public void didFinishSentimentAnalysis(List<Tweet> tweets);

}
