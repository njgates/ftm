package uk.ac.cam.grpproj.juliet.twitter.sentiment;

import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.jfree.data.xy.XYSeries;

import uk.ac.cam.grpproj.juliet.DataPoints;
import uk.ac.cam.grpproj.juliet.db.SentimentAggregateDB;
import uk.ac.cam.grpproj.juliet.financial.Index;

public class ProcessSentiment { 
	public static XYSeries getSentimentSeries(XYSeries fData, Index index)
			throws SQLException, DataPoints.IllegalIntervalException {
		XYSeries sentiment = new XYSeries("Sentiment");
		
		if (fData.isEmpty()) {
			System.out.println("No financial data for selected period");
		} else {
			sentiment.add(fData.getX(0), 0.0);
		}
		
		SentimentAggregateDB sDB = new SentimentAggregateDB();
		
		for (int i = 1; i < fData.getItemCount(); i++) {
		double mean = sDB.getSentimentForDateRange(
					new Date(fData.getX(i - 1).longValue()), new Date(fData
							.getX(i).longValue() - 1000), index);
			
			sentiment.add(fData.getX(i), mean
					+ sentiment.getY(i - 1).doubleValue());
			//sentiment.add(fData.getX(i), mean);
		}
		sDB.close();
		return sentiment;
	}

	/*
	 * private static Double getMultiplierForRetweets(long retweets) { //If
	 * linear then //Plus 1 so as not to get zero all the time!! return
	 * Double.valueOf(retweets) + 1;
	 * 
	 * //TODO make this exponential? Log? }
	 */
}
